import {ResolverMap} from "../utils/resolver-map";
import {Category} from "../entity/Category";
import {processDelete, processUpload} from "../utils/FilesHandlers/localFileHandler";

const categoryResolvers: ResolverMap = {
    Query: {
        /**
         * GET CATEGORIES/CATEGORY
         * @param _
         * @param categoryId
         * @access: PUBLIC
         */
        "categories": async (_, {categoryId}: GQL.ICategoriesOnQueryArguments, {}) => {
            const categories = await Category.find({
                where: categoryId ? {_id: categoryId} : "",
                order: {
                  categoryName: "ASC"
                },
                relations: ["subcategories", "listings"]
            });

            if (!categories) return null;
            return categories;
        }
    },
    Mutation: {
        /**
         * CREATE CATEGORY
         * @param _
         * @param categoryName
         * @param categoryImage
         * @access: ADMIN
         */
        "createCategory": async (_, {data}: GQL.ICreateCategoryOnMutationArguments, {}) => {

            const {categoryName, image, inputs} = data;
            console.log(inputs);

            const pictureUrl = await processUpload(image, "categories");

            const newCategory = await Category.create({
                categoryName,
                categoryImage: pictureUrl,
                inputsIdentifier: categoryName+ "_inputs",
                inputs: inputs || ""
            });

            await newCategory.save();
            return newCategory;
        },
        /**
         * UPDATE CATEGORY
         * @param _
         * @param newCategoryImage
         * @param categoryId
         * @param newCategoryName
         * @access: ADMIN
         */
        "updateCategory": async (_, {data: {newCategoryImage, categoryId, newCategoryName}}: GQL.IUpdateCategoryOnMutationArguments, {}) => {

         const category = await Category.findOne({where: {_id: categoryId}, relations: ["subcategories"]});

         if (!category) throw new Error('No category');
         let pictureUrl: string | null = null;
         if(newCategoryImage){
             pictureUrl = await processUpload(newCategoryImage, "categories");
             await processDelete(category.categoryImage);
         }

         const updatedCategory = Category.create(category);
         updatedCategory.categoryName = newCategoryName ? newCategoryName : category.categoryName;
         updatedCategory.categoryImage = pictureUrl ? pictureUrl : category.categoryImage;

         await updatedCategory.save();
         return updatedCategory;
        },
        /**
         * DELETE CATEGORY
         * @param _
         * @param categoryId
         * @access: ADMIN
         */
        "deleteCategory": async (_, {categoryId}: GQL.IDeleteCategoryOnMutationArguments) => {
            const category = await Category.findOne({where: {_id: categoryId}, relations: ["subcategories"]});
            if(!category) return false;
            await processDelete(category.categoryImage);
            await category?.remove();
            return true;
        }
    }
};

export {categoryResolvers};