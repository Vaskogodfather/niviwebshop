import {BaseEntity, Column, Entity, OneToMany, PrimaryGeneratedColumn} from "typeorm";
import {Subcategory} from "./Subcategory";
import {Listing} from "./Listing";

@Entity("categories")
export class Category extends BaseEntity {
    @PrimaryGeneratedColumn("uuid")
    _id: string;

    @Column("varchar", { length: 255 })
    categoryName: string;

    @Column("text")
    categoryImage: string;

    @Column("varchar", { length: 255, nullable: true, unique: true })
    inputsIdentifier: string;

    @Column("text")
    inputs: JSON;

    @OneToMany(_type => Subcategory, subcategory => subcategory.category)
    subcategories: Subcategory[];

    @OneToMany(_type => Listing, listing => listing.category)
    listings: Listing[];
}