import { RouteComponentProps } from "react-router-dom";
import React from "react";
import {ListingsCard} from "./ListingsCard";
import {ListingsHeader} from "./ListingsHeader";
import {ListingsSidebar} from "./ListingsSidebar";
import {useListingsQuery} from "../../generated/graphql";

interface Props extends RouteComponentProps<{id: string}>{}

export const ListingsPage: React.FC<Props> = ({match}) => {

    const listings = useListingsQuery({
        fetchPolicy: 'network-only',
        variables: {
            subcategoryId: match.params.id
        }
    });

    if(listings.loading) return <div>Loading...</div>;
    if(listings.error) return <div>404</div>;

    return(
        <div className="ads-results-container">

            <div>
            <ListingsHeader />

                {listings?.data?.listings?.map(listing => (
                        <ListingsCard key={listing._id} payload={listing}/>
                    ))}

            </div>

            <ListingsSidebar />
        </div>
    )
};