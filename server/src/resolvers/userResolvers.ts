import {ResolverMap} from "../utils/resolver-map";
import * as bcrypt from "bcryptjs";
import {validatePasswordInput, validateRegistrationInput} from "../validations/authValidations";
import {parseError} from "../utils/error-parser";
import {User} from "../entity/User";
import {
    emailTakenStringError,
    userNotAuthenticatedError,
    invalidCredentialsError,
    emailNotConfirmedError, userNotFoundError, expiredKeyError
} from "../utils/error-messages";
import {sendEmail} from "../utils/auth/send-confirmation-email";
import {createConfirmationEmailLink} from "../utils/auth/createConfirmationEmailLink";
import {createAccessToken} from "../utils/auth/tokenHandler";
import {createForgotPasswordLink} from "../utils/auth/createForgotPasswordLink";
import Constants from "../constants/Constants";
import {getConnection} from "typeorm";

const userResolvers: ResolverMap = {

    Query: {
        test: async () => {
          return "Hello World from server";
        },
        /**
         * GET CURRENT USER IF LOGGED IN
         * @param _
         * @param __
         * @param request
         * @access private
         */
        me: async (_, __, {request}) => {

            if(!request.req.isAuth) {
                return {
                    user: null,
                    errors: [
                        {
                            path: userNotAuthenticatedError.path,
                            message: userNotAuthenticatedError.message
                        }
                    ]
                }
            }

            const user = await User.findOne({where: {_id: request.req.userId}});

            return {
                user: user,
                errors: null
            }
        },
        users: async (_, __, {}) => {

            const users = await getConnection()
                .getRepository(User)
                .createQueryBuilder("user")
                .leftJoinAndSelect("user.listings", "listings")
                .getMany();

            if (!users) {
                return {
                    users: null,
                    errors: [
                        {
                            path: "user",
                            message: "No users found"
                        }
                    ]
                }
            }

            return {
                users: users,
                errors: null
            }
        }
    },
    Mutation: {
        /**
         * LOGIN USER
         * @param _
         * @param email
         * @param password
         * @access public
         */
        loginUser: async (_, {email, password}: GQL.ILoginUserOnMutationArguments) => {

            const user = await User.findOne({where: { email }});

            if(!user) {
                return {
                    user: null,
                    token: null,
                    errors: [
                        {
                            path: invalidCredentialsError.path,
                            message: invalidCredentialsError.message
                        }
                    ]
                }
            }

            const valid = await bcrypt.compare(password, user.password);

            if(!valid) {
                return {
                    user: null,
                    token: null,
                    errors: [
                        {
                            path: invalidCredentialsError.path,
                            message: invalidCredentialsError.message
                        }
                    ]
                }
            }

            if(!user.emailConfirmed) {
                return {
                    user: null,
                    token: null,
                    tokenExpiration: null,
                    errors: [
                        {
                            path: emailNotConfirmedError.path,
                            message: emailNotConfirmedError.message
                        }
                    ]
                }
            }

            const accessToken = await createAccessToken(user);

            return {
                user: user,
                token: accessToken,
                errors: null
            };
        },
        /**
         * REGISTER NEW USER
         * @param _
         * @param data
         * @access public
         */
        createUser: async (_, {data}: GQL.ICreateUserOnMutationArguments, {redis, url}) => {

            const registerValidation = validateRegistrationInput();

            try {
                await registerValidation.validate(data, {abortEarly: false});
            } catch (err) {
                return {
                    user: null,
                    token: null,
                    errors: parseError(err)
                }
            }

            const { email, password, firstName, lastName, city, phone } = data;

            const emailExists = await User.findOne({
                where: {email},
                select: ["_id"]
            });

            if (emailExists) {
                return {
                    user: null,
                    token: null,
                    errors: [
                        {
                            path: emailTakenStringError.path,
                            message: emailTakenStringError.message
                        }
                    ]
                }
            }

            const hashedPassword = await bcrypt.hash(password, 12);

            const newUser = await User.create({
                email,
                password: hashedPassword,
                firstName,
                lastName,
                city,
                phone: phone.toString(),
                last_seen: Date()
            });

            await newUser.save();

            await sendEmail(email, await createConfirmationEmailLink(url, newUser._id, redis));

            return {
                user: newUser,
                token: null,
                errors: null
            };
        },
        sendForgotPasswordEmail: async (_, {email}: GQL.ISendForgotPasswordEmailOnMutationArguments, {redis}) => {

            const user = await User.findOne({where: {email: email}});
            if (!user) {
                return {
                    path: userNotFoundError.path,
                    message: userNotFoundError.message
                }
            }

            // await forgotPasswordLockAccount(user._id);
            // TODO - add frontend url
            // const url = await createForgotPasswordLink(Constants.frontendHost, user._id, redis);
            // TODO send email with url
            await sendEmail(email, await createForgotPasswordLink(Constants.frontendHost, user._id, redis));

            return true
        },
        changeForgottenPassword: async (_, {newPassword, key}: GQL.IChangeForgottenPasswordOnMutationArguments, {redis}) => {

            const redisKey = `${Constants.forgotPasswordPrefix}${key}`;

            const userId = await redis.get(redisKey);

            if(!userId) {
                return {
                    user: null,
                    errors: [
                        {
                            path: expiredKeyError.path,
                            message: expiredKeyError.message
                        }
                    ]
                }
            }

            const user = await User.findOne({where: {_id: userId}});

            if(!user) {
                return {
                    user: null,
                    errors: [
                        {
                            path: userNotFoundError.path,
                            message: userNotFoundError.path
                        }
                    ]
                }
            }

            const passwordValid = validatePasswordInput();

            try {
                await passwordValid.validate({newPassword}, {abortEarly: false});
            } catch (err) {
                return {
                    user: null,
                    errors: parseError(err)
                }
            }

            const hashedPassword = await bcrypt.hash(newPassword, 12);

            const updatePromise = User.update({ _id: userId }, {
                password: hashedPassword
            });

            const deleteKeyPromise = redis.del(redisKey);

            await Promise.all([updatePromise, deleteKeyPromise]);

            return {
                user: user,
                errors: null
            };
        },
        updateOnlineStatus: async (_, __, {request, pubsub}: any, _info: any) => {

            if(!request.req.isAuth) {
                return {
                    user: null,
                    errors: [
                        {
                            path: userNotAuthenticatedError.path,
                            message: userNotAuthenticatedError.message
                        }
                    ]
                }
            }

            try {
                const user = await User.update( {_id: request.req.userId}, {
                    last_seen: Date()
                });

                await pubsub.publish('onlineStatus', {
                    onlineStatus: user
                });

                return {
                    user: user,
                    errors: null
                }
            } catch(err) {
                console.log(err);
                throw new Error(err)
            }
        },
    },
    Subscription: {
        onlineStatus: {
            subscribe(_parent: any, _args: any, {pubsub}, _info: any){
                console.log("entered")
                return pubsub.asyncIterator('onlineStatus')
            }
        }, 
        hello: {
            subscribe(_,__,{pubsub}) {
                return pubsub.asyncIterator('hello')
            }
        }
    }
};

export {userResolvers};