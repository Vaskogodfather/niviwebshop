import { Button } from "@material-ui/core";
import React from "react";
import {RouteComponentProps} from "react-router-dom";
import {useListingsQuery} from "../../generated/graphql";
import {ListingsSidebar} from "../ListingsPage/ListingsSidebar";

interface Props extends RouteComponentProps<{id: string}>{}

export const ListingPage: React.FC<Props> = ({match}) => {

    const listing = useListingsQuery({
        variables: {
            listingId: match.params.id
        }
    });

    if (listing.loading) return <div>Loading...</div>;
    if (listing.error) return <div>404</div>;

    let images: Array<any> = [];

        listing?.data?.listings?.map(listing => {
           if(listing.images && listing.images.length > 0){
               const temp = listing.images.split(',');
               temp.map((img: string) => {
                   return images.push(`http://localhost:1111/images/${img}`)
               })
           }
        });

    const payload = listing?.data?.listings ? listing?.data?.listings[0] : null;

    return(
        <div className="ads-results-container">
        <div className="listing-page-container">
            <p className="subcategories-header" style={{marginBottom: '3rem', marginTop: '-2rem'}}>{payload?.name}</p>

            <div className="listing-page-top-container">
                <div className="gallery-container">
                    <img className="gallery-main-image" src={images[0]} alt=""/>
                    <div className="gallery-thumbnail-container">
                        {
                            images.map((image, index) => (
                                index < 4 ?
                                <img  key={index} className="gallery-thumbnail-image" src={image} alt=""/> : null
                            ))
                        }
                    </div>
                </div>
                <div>

                    <div className="listing-header-wrapper">
                    <div className="listing-details-heading">
                        <p >Cena: <span className="listing-price">{payload?.price} {payload?.currency}</span></p>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Enim nisi repellat voluptatibus. Ex nemo quisquam saepe voluptatum. Aut autem consequuntur debitis dignissimos itaque natus quia recusandae sequi. Dignissimos, incidunt voluptates.</p>
                    </div>

                    <div className="listing-details-container">

                        <div className="listing-seller-data">
                            <h3>Prodavac:</h3>
                            <p>Petar Petrovic</p>
                            <p>Grad: Novi Sad</p>
                            <p>Telefon: <a style={{textDecoration: 'none'}} href="#">021/223-441</a></p>
                            <Button variant="outlined" style={{width: '15rem', letterSpacing: '1px', marginTop: '1rem', color: 'green'}} type="submit">Posalji poruku</Button>
                        </div>

                        <div className="listing-banner-container">
                        <img src={require('../../images/temp/banka.jpg')} alt="logo" className="listing-banner"/>
                        <Button variant="outlined" style={{width: '15rem', letterSpacing: '1px', marginTop: '1rem', float: 'right', color: ''}} type="submit">Online kredit</Button>
                        </div>
                        </div>
                    </div>
                </div>
            </div>

            <div>
            <p className="subcategories-header" style={{marginBottom: '3rem', marginTop: '-2rem'}}>Opis oglasa</p>
                {payload?.description}
            </div>

            <div>
                Pitanja i odgovori
            </div>

            <div>Slicni oglasi</div>

        </div>
    <ListingsSidebar />
</div>
)
};