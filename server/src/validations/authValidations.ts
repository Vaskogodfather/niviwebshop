import * as yup from "yup";
import {
    emailInvalidError,
    emailTooLongError,
    emailTooShortError,
    passwordTooLongError,
    passwordTooShortError
} from "../utils/error-messages";

export function validateRegistrationInput() {
    return yup.object().shape({
        email: yup.string().min(3, emailTooShortError.message).max(255, emailTooLongError.message).email(emailInvalidError.message),
        password: yup.string().min(6, passwordTooShortError.message).max(255, passwordTooLongError.message)
    });
}

export function validatePasswordInput() {
    return yup.object().shape({
        newPassword: yup.string().min(6, passwordTooShortError.message).max(255, passwordTooLongError.message)
    });
}