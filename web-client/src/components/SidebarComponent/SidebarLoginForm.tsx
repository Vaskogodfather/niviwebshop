import React, {useState} from "react";
import {useLoginMutation} from "../../generated/graphql";
import {setAccessToken} from "../../auth/accessToken";
import {Link} from "react-router-dom";

interface Props {
    refetch: Function
}

export const SidebarLoginForm: React.FC<Props> = (props) => {
    const [loginEmail, setLoginEmail] = useState('');
    const [loginPassword, setLoginPassword] = useState('');
    const [login] = useLoginMutation();

    return(
        <div>
            <form onSubmit={async e => {
                e.preventDefault();
                const response = await login({
                    variables: {
                        loginEmail,
                        loginPassword
                    }
                });

                if (response && response.data && response.data.loginUser && response.data.loginUser.token) {
                    setAccessToken(response.data.loginUser.token);
                    localStorage.setItem('token', `Bearer ${response.data.loginUser.token}`)
                }

                await props.refetch();
            }}>

                <div className="sidebar-header">
                    <input className="sidebar-header__input" value={loginEmail}  placeholder="Email" type="text" onChange={e => {
                        setLoginEmail(e.target.value) }}/>

                    <input className="sidebar-header__input" value={loginPassword} placeholder="Password" type="text" onChange={e => {
                        setLoginPassword(e.target.value)
                    }}/>
                    <button className="sidebar-header__button" type="submit">Login</button>
                    <Link to="/auth" className="sidebar-header__register">Register</Link>
                </div>
            </form>
        </div>
    )
}