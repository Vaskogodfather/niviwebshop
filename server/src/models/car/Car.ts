import {InputTypes, ListingModel} from "../shared/Listing";
import {AdditionalEquipment, CarBody, FuelTypes} from "./CarLists";

export const CarListingInputs: Array<ListingModel> = [
    {inputName: 'Karoserija', inputValue: 'Karoserija', inputType: InputTypes.DROPDOWN_LIST, inputList: CarBody, required: true, serchable: true},
    {inputName: 'Kubikaza', inputValue: 'Kubikaza', inputType: InputTypes.NUMBER, required: true, serchable: true},
    {inputName: 'Gorivo', inputValue: 'Gorivo', inputType: InputTypes.DROPDOWN_LIST, inputList: FuelTypes, required: true, serchable: true},
    {inputName: 'Dodatna oprema', inputValue: 'DodatnaOprema', inputType: InputTypes.CHECKBOX_LIST, inputList: AdditionalEquipment, required: false, serchable: true}
];

// export const CarSearchInputs: Array<ListingModel> = [
//     {inputName: 'Karoserija', inputValue: 'Karoserija', inputType: InputTypes.DROPDOWN_LIST, inputList: CarBody, required: false},
//     {inputName: 'Kubikaza', inputValue: 'Kubikaza', inputType: InputTypes.NUMBER, required: false},
//     {inputName: 'Gorivo', inputValue: 'Gorivo', inputType: InputTypes.DROPDOWN_LIST, inputList: FuelTypes, required: false},
//     {inputName: 'Dodatna oprema', inputValue: 'DodatnaOprema', inputType: InputTypes.CHECKBOX_LIST, inputList: AdditionalEquipment, required: false}
// ];
