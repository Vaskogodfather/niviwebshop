import {
    Query,
    useCategoriesWithSubcategoriesQuery,
    useCreateListingMutation,
    useGetAddListingInputsQuery
} from "../../generated/graphql";
import { RouteComponentProps, withRouter } from "react-router-dom";
import React, {useEffect, useMemo, useState} from "react";
import {useDropzone} from "react-dropzone";
import {acceptStyle, activeStyle, baseStyle, rejectStyle} from "../../styles/imageUploadDialogStyle";
import {CustomSelectItem} from "../../components/Forms/InputFields/CustomSelectItem";
import {generateFormFromJSON} from "../../components/Forms/generateFormFromJSON";
import {CustomTextInput} from "../../components/Forms/InputFields/CustomTextInput";
import DetailsPresentationComponent from "../../components/DetailsPresentationComponent/DetailsPresentationComponent";

interface Props extends RouteComponentProps{
    refetch: Function,
    session: Query,
}

const valute: any[] = [
    {value: "din", label: "Din"},
    {value: "eur", label: "Euro"},
    {value: "dollar", label: "Dolar"},
];

let categoryItems: any[] = [];
let subcategoryItems: any[] = [];

// const sleep = (time: number) => new Promise((accepted) => setTimeout(accepted, time));

const initialValues = {
    category: '',
    subcategory: '',
    name: '',
    description: '',
    price: 0,
    currency: '',
    details: {}
};

const CreateListingPage: React.FC<Props> = (props) => {

    const {data, loading, error} = useCategoriesWithSubcategoriesQuery({fetchPolicy: 'network-only'});
    const [categoryIdentifier, setCategoryIdentifier] = useState('');
    const [subcategoryIdentifier, setSubcategoryIdentifier] = useState('');
    const [files, setFiles] = useState([]);
    const [createListing] = useCreateListingMutation();
    const [listingValues, setListingValues] = useState(initialValues);
    const [step, setStep] = useState(0);
    let customInputForms;
    var detailsArr: any = [];

    const {acceptedFiles, getRootProps, getInputProps, isDragAccept, isDragReject, isDragActive} = useDropzone({
        accept: 'image/*',
        maxSize: 500000,
        maxFiles: 10,
        onDrop: acceptedFiles => {
            // @ts-ignore
            setFiles(acceptedFiles.map(file => Object.assign(file, {
                preview: URL.createObjectURL(file)
            })));
        }
    });

    const acceptedFileItems = acceptedFiles.map((file: any) => (
        <div className="thumb" key={file.name}>
            <div className="thumbInner">
                <img className="upload-image-preview" src={file.preview} alt=""/>
            </div>
        </div>
    ));

    const style = useMemo(() => ({
        ...baseStyle,
        ...(isDragActive ? activeStyle : {}),
        ...(isDragAccept ? acceptStyle : {}),
        ...(isDragReject ? rejectStyle : {})
    }), [isDragActive, isDragReject, isDragAccept]);

    useEffect(() => () => {
        // @ts-ignore
        files.forEach(file => URL.revokeObjectURL(file.preview));
    }, [files]);


    const customInputs = useGetAddListingInputsQuery({
        fetchPolicy: 'network-only',
        variables: {
            categoryInputId: categoryIdentifier,
            subcategoryInputId: subcategoryIdentifier
        }
    });

    if (loading) return (<div>Loading</div>);
    if (error) props.history.push('/');

    const getSubcategoriesForCategory = async (e: Event) => {
        if (data && data.categories) {
            subcategoryItems = [];
            if(listingValues.category.length > 0) {
                setListingValues({
                    ...listingValues,
                    details: {}
                });
            }
            await data.categories.map(category => {
                if (category._id === ((e.target as HTMLTextAreaElement).value)) {
                    setCategoryIdentifier(category?.inputsIdentifier ? category.inputsIdentifier : '');
                    category?.subcategories?.map(subcategory => {
                         setSubcategoryIdentifier(subcategory.inputsIdentifier ? subcategory.inputsIdentifier : '');
                         return subcategoryItems.push({label: subcategory.subcategoryName, value: subcategory._id})
                    })
                }
                return true;
            });
        }
    };

    if (data && data.categories) {
        categoryItems = [];
        data.categories.map(category => {
            return categoryItems.push({
                label: category.categoryName,
                value: category._id,
                execute: getSubcategoriesForCategory
            })
        })
    }

    if(!customInputs.loading && customInputs.data && customInputs.data.getAddListingInputs){
        customInputForms = generateFormFromJSON(listingValues, setListingValues,step, customInputs.data?.getAddListingInputs);
    }

    const isFormValid = () => {
        let stepOneValid = listingValues.category.length > 0 && listingValues.subcategory.length > 0;
        let stepTwoValid = listingValues.name.length > 0 && listingValues.price !== 0 && listingValues.currency.length > 0;
        if(step === 0) return stepOneValid;
        if(step === 1) return  stepTwoValid;
        return true
    };

    return (
        <div className="create-listing-container">
            <p className="subcategories-header" style={{color: 'grey', marginBottom: '3rem'}}>Kreiranje oglasa</p>
            <div className="step-bar-container">
                <ul className="step-bar">
                    <li className={`step-bar__item step-bar__item_active`}><p>Kategorija</p></li>
                    <li className={`step-bar__item ${step > 0 ? 'step-bar__item_active' : ''}`}><p>Oglas&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p></li>
                    <li className={`step-bar__item ${step > 1 ? 'step-bar__item_active' : ''}`}><p>Detalji&nbsp;&nbsp;&nbsp;&nbsp;</p></li>
                    <li className={`step-bar__item ${step > 2 ? 'step-bar__item_active' : ''}`}><p>Pregled&nbsp;&nbsp;&nbsp;&nbsp;</p></li>
                </ul>
            </div>
        <form onSubmit={async (e) => {
            e.preventDefault();
            if(step !== 3){
                setStep(step + 1);
            } else {
                await createListing({
                    variables: {
                        name: listingValues.name,
                        description: listingValues.description,
                        price: parseInt(String(listingValues.price)),
                        currency: listingValues.currency,
                        images: acceptedFiles ? acceptedFiles : null,
                        category: listingValues.category,
                        subcategory: listingValues.subcategory,
                        details: JSON.stringify(listingValues.details)
                    }
                });

            }
        }} className="create-listing-page-container">

            <div
                className="create-listing-form-step"
                style={{display: `${step === 0 ? '' : 'none'}`}}>
            <CustomSelectItem
                itemName="category"
                items={categoryItems}
                setListingValues={setListingValues}
                placeholder="Kategorija"
                listingValues={listingValues}
                fieldToInvalidate ="subcategory"
                execute={getSubcategoriesForCategory}/>

            <CustomSelectItem
                itemName="subcategory"
                items={subcategoryItems}
                setListingValues={setListingValues}
                placeholder="Podkategorija"
                listingValues={listingValues}/>
            </div>

            <div
                style={{display: `${step === 1 ? '' : 'none'}`}}
                className="create-listing-form-step">
                <CustomTextInput
                    val={listingValues.name}
                    name={listingValues.name}
                    inputName = "name"
                    placeholder="Ime oglasa"
                    typeOfInput="text"
                    listingValues={listingValues}
                    setListingValues={setListingValues}
                />

                <CustomTextInput
                    val={listingValues.description}
                    name={listingValues.description}
                    inputName = "description"
                    placeholder="Opis oglasa"
                    typeOfInput="text"
                    listingValues={listingValues}
                    setListingValues={setListingValues}
                />

                <div className="create-listing-price-value-container">
            <CustomTextInput
                val={listingValues.price}
                name={listingValues.price}
                inputName = "price"
                placeholder="Cena"
                typeOfInput="number"
                listingValues={listingValues}
                setListingValues={setListingValues}
            />

            <CustomSelectItem
                itemName="currency"
                items={valute}
                setListingValues={setListingValues}
                listingValues={listingValues}/>

                </div>

            <section>
                <div {...getRootProps(
                    //@ts-ignore
                    {style}
                )}>
                    <input {...getInputProps()} />
                    {isDragAccept && (<p>All files will be accepted</p>)}
                    {isDragReject && (<p>Some files will be rejected</p>)}
                    {!isDragActive && (<p>Dodaj novu sliku ...</p>)}
                </div>
                <aside className="thumb-container">
                    <ul>{acceptedFileItems}</ul>
                </aside>
            </section>
            </div>

            {customInputForms}

            <div style={{display: `${step === 3 ? '' : 'none'}`}} >
                <div className="create-listing-preview">
                <h3>Ime oglasa</h3>
                <p>{listingValues.name}</p>
                <h3>Opis</h3>
                <p>{listingValues.description}</p>
                <h3>Cena</h3>
                <p>{parseInt(String(listingValues.price)) + ' ' + listingValues.currency}</p>
                </div>

                <DetailsPresentationComponent jsonList={listingValues.details}/>
            </div>

            <div className="create-listing-buttons-wrapper">
            <button
                className="crete-listing-back-button"
                disabled={step === 0}
                onClick={e => {
                e.preventDefault();
                if(step !== 0){
                    setStep(step - 1);
                }
            }}>Nazad</button>

            <button
                disabled={!isFormValid()}
                className="create-listing-next-button"
                type="submit">{step === 3 ? 'Dodaj oglas' : 'Dalje'}</button>
            </div>

        </form>
        </div>
    )
};

export default withRouter(CreateListingPage);
