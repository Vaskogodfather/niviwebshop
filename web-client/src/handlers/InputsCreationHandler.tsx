import {InputTypes} from "../entities/InputTypes";
import DropdownInputCreationComponent from "../components/Forms/InputCreationComponents/DropdownInputCreationComponent";
import CheckboxInputCreationComponent from "../components/Forms/InputCreationComponents/CheckboxInputCreationComponent";
import TextInputCreationComponent from "../components/Forms/InputCreationComponents/TextInputCreationComponent";
import MultilineTextInputCreationComponent
    from "../components/Forms/InputCreationComponents/MultilineTextInputCreationComponent";
import NumberInputCreationComponent from "../components/Forms/InputCreationComponents/NumberInputCreationComponent";
import {InputModel} from "../entities/InputModel";

let inputs: InputModel[] = [];

export const createNewInput = (inputType: InputTypes) => {
    switch (inputType) {
        case InputTypes.TEXT: return createTextInputType();
        case InputTypes.NUMBER: return createNumberInputType();
        case InputTypes.MULTILINE_TEXT: return createMultilineTextInputType();
        case InputTypes.DROPDOWN_LIST: return createDropdownListInputType();
        case InputTypes.CHECKBOX_LIST: return createCheckboxListInputType();
        default:return;
    }
};

const addToInputs = async (inputToAdd: InputModel) => {
    console.log('input to add')
    // @ts-ignore
    console.log(inputToAdd);

    let exists = false;
    await inputs.map(input => {
        if (input.value === inputToAdd.value) exists = true;
        return
    });
    if (!exists) inputs.push(inputToAdd);
    console.log(JSON.stringify(inputs));
};

const removeFromInputs = async (inputToRemove: InputModel) => {
    await inputs.map((input, index) => {
        if (input.value == inputToRemove.value) {
            inputs.splice(index, 1);
        }
    });
    console.log(JSON.stringify(inputs));
};

export const returnAllInputs = () => {
    let inputsToReturn = inputs;
    inputs = [];
    return inputsToReturn;
};

const createTextInputType = () => {
    return (<TextInputCreationComponent addToInputs={addToInputs} removeFromInputs={removeFromInputs}/>);
};

const createMultilineTextInputType = () => {
    return <MultilineTextInputCreationComponent addToInputs={addToInputs} removeFromInputs={removeFromInputs}/>;
};

const createNumberInputType = () => {
    return (<NumberInputCreationComponent addToInputs={addToInputs} removeFromInputs={removeFromInputs}/>);
};

const createDropdownListInputType = () => {
    return (<DropdownInputCreationComponent addToInputs={addToInputs} removeFromInputs={removeFromInputs}/>);
};

const createCheckboxListInputType = () => {
    return (<CheckboxInputCreationComponent addToInputs={addToInputs} removeFromInputs={removeFromInputs}/>)
};
