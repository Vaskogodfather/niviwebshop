import {BaseEntity, Column, Entity, JoinTable, ManyToOne, OneToMany, PrimaryGeneratedColumn} from "typeorm";
import {Category} from "./Category";
import {Listing} from "./Listing";

@Entity("subcategories")
export class Subcategory extends BaseEntity {

    @PrimaryGeneratedColumn("uuid")
    _id: string;

    @Column("varchar", { length: 255 })
    subcategoryName: string;

    @Column("text")
    subcategoryImage: string;

    @Column("varchar", { length: 255, nullable: true, unique: true })
    inputsIdentifier: string;

    @Column("text")
    inputs: JSON;

    @ManyToOne(_type => Category, category => category.subcategories, { cascade: true })
    @JoinTable()
    category: Category;

    @OneToMany(_type => Listing, listings => listings.subcategory)
    listings: Listing[];

}