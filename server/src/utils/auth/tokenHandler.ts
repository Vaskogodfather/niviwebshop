import {User} from "../../entity/User";
import * as jwt from "jsonwebtoken";
import Constants from "../../constants/Constants";
// import {Response} from "express";

export const createAccessToken = (user: User) => {
    return jwt.sign({userId: user._id, email: user.email}, Constants.hashStringAccessToken, {expiresIn: '1y'});
};

export const createRefreshToken = (user: User) => {
    return jwt.sign({userId: user._id, email: user.email, tokenVersion: user.tokenVersion}, Constants.hashStringRefreshToken, {expiresIn: '1y'});
};

// export const sendRefreshToken = (res: Response, token: string) => {
//     res.cookie('jid', token, {
//         httpOnly: true,
//         path: '/refresh_token'
//     });
// }