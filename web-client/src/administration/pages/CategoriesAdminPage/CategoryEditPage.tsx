import React from 'react';
import {Link, RouteComponentProps, withRouter} from "react-router-dom";
import {
    CategoryWithSubcategoriesDocument,
    Query,
    useCategoryWithSubcategoriesQuery,
    useDeleteSubcategoryMutation
} from "../../../generated/graphql";

interface Props extends RouteComponentProps<{id: string}>{
    session: Query
}

const CategoriesEditPage: React.FC<Props> = (props) => {

    const [deleteSubcategory] = useDeleteSubcategoryMutation();
    const {loading, data: categories} = useCategoryWithSubcategoriesQuery({
        fetchPolicy: 'network-only',
        variables: {
            categoryId: props.match.params.id
        }
    });
    if(loading) return <div>Loading</div>;
    if(!categories?.categories) return <div>404</div>;

    return(
        <div className="admin-container">
            <h1 className="category-card__header">{`Edit ${categories?.categories[0]?.categoryName} category`}</h1>
            <Link to={`/create-subcategory/${categories?.categories[0]?._id}`} className="category-item-admin-button--change" style={{margin: 'auto', textAlign: 'center'}}>Create subcategory</Link>
            {
                categories?.categories[0].subcategories?.map(subcategory => {
                    return(
                        <div className="category-item-admin-container" key={subcategory._id}>
                            <p className="category-item-admin-paragraph"><strong>Category: </strong>{subcategory.subcategoryName}</p>
                            <img src={`http://localhost:1111/images/${subcategory.subcategoryImage}`} alt="logo" className="category-card__image"/>
                            <div>{``}</div>

                            <Link to={`/edit-subcategory/${subcategory._id}`} className="category-item-admin-button--change">Izmeni podkategoriju</Link>

                            <button
                                className="category-item-admin-button--delete"
                                onClick={async event => {
                                    event.preventDefault();
                                    event.stopPropagation();
                                    await deleteSubcategory({
                                        variables: {
                                            id: subcategory._id
                                        },
                                        refetchQueries: [{query: CategoryWithSubcategoriesDocument}]
                                    });
                                }}
                            >Izbrisi podkategoriju</button>
                        </div>
                    )
                })
            }
        </div>

    )
};

export default withRouter(CategoriesEditPage);