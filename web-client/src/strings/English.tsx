
export default {
    languageName: 'English',

    // Authentication
    Authentication: 'Authentication',
    Register: 'Register',
    Login: 'Login',
    Password: 'password',
    TermsOfUse: 'Terms of use',

    //Header
    HeaderAddSearch: 'Search ads',

    // Settings
    selectLanguage: 'Select language'

}