import React from "react";
import IconButton from "@material-ui/core/IconButton";
import FormatListBulletedIcon from "@material-ui/icons/FormatListBulleted";
import AppButton from "@material-ui/icons/Apps";

export const ListingsHeader: React.FC = () => {
    return (
        <div className="ads-results-container__header">
            <h3 className="ads-results-container__header__header">Prikaz 1 do 30 nadjenih</h3>
            <IconButton aria-label="delete">
                <FormatListBulletedIcon style={{fontSize: '2rem'}}/>
            </IconButton>
            <IconButton aria-label="delete">
                <AppButton style={{fontSize: '2rem'}}/>
            </IconButton>
            <h3 className="ads-results-container__header__sort-by">Sortiraj po</h3>
        </div>
    )
}