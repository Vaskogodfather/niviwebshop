
export default {

    languageName: 'Serbian',

    // Authentication
    Authentication: 'Autentikacija',
    Register: 'Registracija',
    Login: 'Ulogujte se',
    Password: 'šifra',
    TermsOfUse: 'Uslovi korišćenja',

    //Header
    HeaderAddSearch: 'Pretraga oglasa',

    // Settings
    selectLanguage: 'Izaberite jezik'

}