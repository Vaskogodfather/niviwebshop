export enum InputTypes{
    TEXT = "TEXT",
    MULTILINE_TEXT = "MULTILINE_TEXT",
    NUMBER = "NUMBER",
    DROPDOWN_LIST = "DROPDOWN_LIST",
    CHECKBOX_LIST = "CHECKBOX_LIST",
    RADIO_LIST = "RADIO_LIST"
}

export type ListingModel = {
    inputName: String,
    inputValue: String,
    inputType: InputTypes,
    inputList?: Array<any>,
    required: boolean,
    serchable : boolean
}