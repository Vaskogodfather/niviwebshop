import {BaseEntity, Column, Entity, JoinTable, ManyToOne, PrimaryGeneratedColumn} from "typeorm";
import {User} from "./User";
import {Subcategory} from "./Subcategory";
import {Category} from "./Category";

@Entity("listings")
export class Listing extends BaseEntity {
    @PrimaryGeneratedColumn("uuid")
    _id: string;

    @Column("varchar", { length: 255 })
    name: string;

    @Column("text")
    description: string;

    @Column("integer")
    price: number;

    @Column("varchar", { length: 255 })
    currency: string;

    @Column("text")
    images: string;

    @Column("simple-json", {nullable: true})
    details: JSON;

    @ManyToOne(_type => User, user => user.listings, {
        cascade: true
    })
    @JoinTable()
    owner: User;

    @ManyToOne(_type => Subcategory, subcategory => subcategory.listings, {
        cascade: true
    })
    @JoinTable()
    subcategory: Subcategory;

    @ManyToOne(_type => Category, category => category.listings, {
        cascade: true
    })
    @JoinTable()
    category: Category;

}