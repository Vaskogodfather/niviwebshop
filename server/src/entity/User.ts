import {Entity, Column, BaseEntity, PrimaryGeneratedColumn, OneToMany} from "typeorm";
import {Listing} from "./Listing";

@Entity("users")
export class User extends BaseEntity {

    @PrimaryGeneratedColumn("uuid")
    _id: string;

    @Column("varchar", {length: 255})
    email: string;

    @Column("varchar", {length: 255})
    firstName: string;

    @Column("varchar", {length: 255})
    lastName: string;

    @Column("varchar", {length: 255})
    phone: string;

    @Column("varchar", {length: 255})
    city: string;

    @Column("text")
    password: string;

    @Column("boolean", {default: false})
    emailConfirmed: boolean;

    @Column("int", {default: 0})
    tokenVersion: number;

    @Column("datetime")
    last_seen: string;

    @OneToMany(_type => Listing, listing => listing.owner)
    listings: Listing[];
}
