import React from 'react';
import {BrowserRouter as Router, Switch, Route} from 'react-router-dom';
import {ApolloClient, ApolloProvider, InMemoryCache, split} from "@apollo/client";
import {WebSocketLink} from "@apollo/link-ws";
import {createUploadLink} from "apollo-upload-client";
import {getMainDefinition} from "@apollo/client/utilities";
import {setContext} from "@apollo/link-context";
import './App.css';
import withSession from './auth/withSession';
import {HomePage} from "./pages/HomePage/HomePage";
import {AdminPanel} from "./administration/pages/AdminPanelPage/AdminPanel";
import AuthPage from "./pages/AuthPage/AuthPage";
import HeaderComponent from "./components/HeaderComponent/HeaderComponent";
import {Language, TRoot} from "./types/CustomTypes";
import {SideBarComponent} from "./components/SidebarComponent/SideBar";
import English from "./strings/English";
import {SettingsPage} from "./pages/SettingsPage/SettingsPage";
import {CategoriesAdminPage} from "./administration/pages/CategoriesAdminPage/CategoriesAdminPage";
import AddNewCategoryPage from "./administration/pages/CategoriesAdminPage/AddNewCategoryPage";
import {customTypePolicies} from "./graphql/utils/typePolicies";
import CategoriesEditPage from "./administration/pages/CategoriesAdminPage/CategoryEditPage";
import AddNewSubcategoryPage from "./administration/pages/CategoriesAdminPage/AddNewSubcategoryPage";
import {CategoryPage} from "./pages/CategoryPage/CategoryPage";
import {ListingsPage} from "./pages/ListingsPage/ListingsPage";
import CreateListingPage from "./pages/CreateListingPage/CreateListingPage";
import {ListingPage} from "./pages/ListingPage/ListingPage";

const cache = new InMemoryCache({
    typePolicies: customTypePolicies
}), wsLink = new WebSocketLink({
    uri: 'ws://localhost:1111/graphql',
    options: {
        reconnect: true,
        connectionParams: () => ({
            Authorization: localStorage.getItem('token')
        }),
    }
}), httpLink = createUploadLink({uri: 'http://localhost:1111/graphql', credentials: 'same-origin'}), link = split(
    ({query}) => {
        // @ts-ignore
        const {kind, operation} = getMainDefinition(query);
        return kind === 'OperationDefinition' && operation === 'subscription';
    },
    wsLink,
    httpLink,
), authLink = setContext(async (_, {headers}) => {
    const token = localStorage.getItem('token');
    return {
        headers: {
            ...headers,
            authorization: token ? `${token}` : "",
        }
    }
}), client = new ApolloClient({
    // @ts-ignore
    link: authLink.concat(link),
    cache: cache,
    credentials: 'include',
    assumeImmutableResults: true
});

interface Props {}

interface IState extends Language{
    // appLanguage: Object
}

class App extends React.Component<Props, IState> {

    constructor(props: Props) {
        super(props);
        this.state = {
            appLanguage: English
        };
        this.switchLanguage = this.switchLanguage.bind(this);
    }

    switchLanguage = (language: Object) => {
        this.setState(prevState => {
            // let appLanguage = Object.assign({}, prevState.appLanguage);
            let appLanguage = language;
            return {appLanguage}
        })
    };

    render() {

        let Root: ({refetch, session}: TRoot) => void;

        Root = ({refetch, session}) => (
            <Router>
                <div className="container">
                    <HeaderComponent session={session} refetch={refetch} strings={this.state.appLanguage as Language}/>

                    <div className="content">
                        <SideBarComponent session={session} refetch={refetch} />
                        <main className="main-view">
                    <Switch>
                        <Route exact path="/" render={() => <HomePage refetch={refetch} session={session}/>}/>
                        <Route exact path="/auth" render={() => <AuthPage refetch={refetch} strings={this.state.appLanguage}/>}/>
                        <Route exact path="/category/:id" component={CategoryPage}/>
                        <Route exact path="/listings/:id" component={ListingsPage}/>
                        <Route exact path="/listings/display/:id" component={ListingPage}/>
                        <Route exact path="/create-listing" render={() => <CreateListingPage refetch={refetch} session={session}/>}/>

                        <Route exact path="/settings" render={() => <SettingsPage switchLanguage={(e: Object) => this.switchLanguage(e)} strings={this.state.appLanguage as Language}/>}/>
                        <Route exact path="/admin-panel" render={() => <AdminPanel refetch={refetch} session={session}/>}/>
                        <Route exact path="/admin-categories" render={() => <CategoriesAdminPage session={session}/>}/>
                        <Route exact path="/create-category" render={() => <AddNewCategoryPage session={session} />}/>
                        <Route exact path="/edit-category/:id" render={() => <CategoriesEditPage session={session} />}/>
                        <Route exact path="/create-subcategory/:id" render={() => <AddNewSubcategoryPage session={session} />}/>
                    </Switch>
                        </main>

                    </div>

                </div>
            </Router>
        );

        const RootWithSession = withSession(Root);

        return (
            <ApolloProvider client={client}>
                <RootWithSession />
            </ApolloProvider>
        )
    }
}

export default App;