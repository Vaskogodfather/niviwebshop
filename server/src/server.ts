import "reflect-metadata";
import express from 'express';
import http = require('http');
import {ApolloServer, PubSub} from'apollo-server-express';
import RateLimit from "express-rate-limit";
import RateLimitRedisStore from "rate-limit-redis";
import {resolvers} from './resolvers/rootResolver';
import { importSchema } from "graphql-import";
import {json} from 'body-parser';
import {createConnection, getConnectionOptions} from "typeorm";
import * as path from "path";
import isAuth from './utils/auth/check-authentication';
import cors from 'cors';
import Constants from './constants/Constants';
import {redis} from "./utils/redis";
import {confirmEmail} from "./utils/auth/confirmEmail";

try {

    const PORT = Constants.graphqlPORT;
    const typeDefs = importSchema(path.join(__dirname, "./schema/schema.graphql"));

    console.clear();

    (async () => {

        const app = express();
        const pubsub = new PubSub();

        const server = new ApolloServer({
            typeDefs: typeDefs,
            resolvers: resolvers,
            context(request) {
                return {
                    pubsub,
                    request,
                    redis,
                    url: request.req.protocol + "://" + request.req.get("host")
                }
            }
        });

        const apiLimiter = RateLimit({
            store: new RateLimitRedisStore({
                client: redis
            }),
            windowMs: 15 * 60 * 1000, // 15 minutes
            max: 100,
            message: "Too many requests sent from this IP, please try again later"
        });

        app.use(cors({
            origin: "*",
            credentials: true
        }));
        app.use(apiLimiter);
        app.use('/graph', json());
        app.use(isAuth);
        app.use('/images', express.static(path.join(__dirname, 'images')));
        server.applyMiddleware({app});

        app.get("/confirm/:id", confirmEmail);


        // meta for sharing (facebook etc) todo - extract routes to separate files
        app.get('/ssr',(_, res) => res.send(String.raw`
            <html>
                <head>
                    <title>Hello</title>
                </head>
                <body>
                    <div>
                        <h1>Hello</h1>
                    </div>                
                </body>
            </html>
        `));


        const httpServer = http.createServer(app);
        server.installSubscriptionHandlers(httpServer);

        const connectionOptions = await getConnectionOptions(process.env.NODE_ENV);
        await createConnection({...connectionOptions, name: "default"});

        httpServer.listen({ port: PORT }, () => {
                console.log(`Server ready at http://localhost:${PORT}${server.graphqlPath}`);
                console.log(`WebSocket subscriptions ready at ws://localhost:${PORT}${server.subscriptionsPath}`);
            });
    })();

} catch (err) {
    console.log(err);
    throw new Error(err);
}