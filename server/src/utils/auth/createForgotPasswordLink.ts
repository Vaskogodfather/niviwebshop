import { v4 } from "uuid";
import {Redis} from "ioredis";
import Constants from "../../constants/Constants";

export const createForgotPasswordLink = async (url: string, userId: string, redis: Redis) => {
    const id = v4();
    await redis.set(`${Constants.forgotPasswordPrefix}${id}`, userId, "ex", 60*20);
    console.log(`${Constants.forgotPasswordPrefix}${id}`);
    console.log( `${url}/change-password/${id}`);
    return `${url}/change-password/${id}`;
};