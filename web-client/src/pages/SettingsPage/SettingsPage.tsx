import React from "react";
import Serbian from "../../strings/Serbian";
import English from "../../strings/English";

interface Props {
    switchLanguage: Function,
    strings: any
}

export const SettingsPage: React.FC<Props> = (props) => {

    const {strings} = props;

    const Languages = [
        {name: 'Serbian', lang: Serbian},
        {name: 'English', lang: English}
    ];

    return(
        <div>
            <h1>{strings.selectLanguage}</h1>
            <select value={strings?.languageName} className="modal-list-li-wrapper">
            {
                Languages.map(lang => {
                    return (
                            <option key={lang.name}
                                 onClick={() => props.switchLanguage(lang.lang)}
                                 className="modal-list-li-1">{lang.name}</option>
                    )
                })
            }
            </select>
        </div>
    )
};