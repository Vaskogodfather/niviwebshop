import {Query} from "../generated/graphql";
import English from "../strings/English";
import Serbian from "../strings/Serbian";

export type TRoot = {
    refetch: Function
    session: Query
};

export type Language = {
    appLanguage: Object
}