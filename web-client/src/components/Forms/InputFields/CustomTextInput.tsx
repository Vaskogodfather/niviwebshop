import React from "react";

export function CustomTextInput({inputName, placeholder, setListingValues, listingValues, typeOfInput, val}: any) {
    return <input
        className="create-listing-text-input"
        type={typeOfInput}
        value={val}
        placeholder={placeholder}
        onChange={(event => {
            event.preventDefault();
            if(typeOfInput === "number" && (isFinite(parseInt(event.target.value)))) {
                setListingValues({
                    ...listingValues,
                    [inputName]: event.target.value
                })
            } else if (typeOfInput === "text") {
                setListingValues({
                    ...listingValues,
                    [inputName]: event.target.value
                })
            }
        })}/>
}