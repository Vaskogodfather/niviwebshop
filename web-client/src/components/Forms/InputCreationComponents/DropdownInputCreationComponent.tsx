import { useState } from "react";
import {InputModel} from "../../../entities/InputModel";
import {InputTypes} from "../../../entities/InputTypes";

interface Props {
    addToInputs: any,
    removeFromInputs: any
}

const DropdownInputCreationComponent: React.FC<Props> = (props) => {

    const [inputName, setInputName] = useState('');
    const [inputValue, setInputValue] = useState('');
    const [required, setRequired] = useState(false);
    const [searchable, setSearchable] = useState(false);

    const [dropdownListInputs, setDropdownListInputs] = useState([]);
    const [dropdownListValues, setDropdownListValues] = useState([]);

    const createInput = () => {
        let input: InputModel = new InputModel(inputName, inputValue, InputTypes.DROPDOWN_LIST, required, searchable, dropdownListValues);
        props.addToInputs(input);
    };

    const removeInput = () => {
        let input: InputModel = new InputModel(inputName, inputValue, InputTypes.DROPDOWN_LIST, required, searchable, dropdownListValues);
        props.removeFromInputs(input);
    };

    return(
        <div className="inputsCreationCard">
            {/*{console.log(dropdownListValues)}*/}
            <p className="inputsCreationCard__item">Dropdown input</p>

            <input value={inputName} onChange={e => {
                setInputName(e.target.value);
                setInputValue(e.target.value);
            }} className="inputsCreationCard__item" type="text" placeholder="input name"/>

            <button style={{marginBottom:'1rem'}} onClick={() => {
                let uniqueKey = dropdownListInputs.length;
                let input: any[] = [...dropdownListInputs, <input
                    type="text"
                    value={dropdownListValues[uniqueKey]}
                    onChange={e => {
                        let newValue = e.target.value;
                        let arr: string[] = dropdownListValues;
                        arr[uniqueKey] = newValue;
                        // @ts-ignore
                        setDropdownListValues(arr);
                        console.log(dropdownListValues);
                    }}
                    className="inputsCreationCard__item"/>];
                // @ts-ignore
                setDropdownListInputs(input)
            }}>Add item
            </button>

            {dropdownListInputs}

            <div className="inputsCreationCard__item">
                <input checked={required} onChange={() => setRequired(!required)} type="checkbox"
                       name="required"/> Required
            </div>

            <div className="inputsCreationCard__item">
                <input checked={searchable} onChange={() => setSearchable(!searchable)} type="checkbox"
                       name="required"/> Searchable
            </div>

            <button className="inputsCreationCard__item" onClick={() => {
                createInput()
            }}>Confirm input
            </button>

            <button className="inputsCreationCard__item" onClick={() => {
                removeInput()
            }}>Remove input
            </button>

        </div>    );
};

export default DropdownInputCreationComponent;