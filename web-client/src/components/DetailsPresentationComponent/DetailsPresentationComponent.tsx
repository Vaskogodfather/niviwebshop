
interface Props{
    jsonList: any
}



const DetailsPresentationComponent: React.FC<Props> = (props) => {

    function renderDetails(){
        return Object.entries(props.jsonList).map(([key, value], i) => {
            let valueList: any = [];
            if (typeof value === 'object' && value !== null) {
                Object.entries(value).map(([_, value], i) => valueList.push(value));
            }
            return (
                <div key={key} className="create-listing-details-container">
                    <p className="create-listing-details-header subcategories-header" style={{color: 'grey', marginBottom: '3rem'}}>{key}</p>
                    {valueList.length > 0 ? valueList.map((item: any) => {
                        return <p className="create-listing-details-item">{item}</p>
                    }) : value}
                </div>
            )
        })
    }

    return (
            <div className="details-card-create-listing">
                <h2>Detalji</h2>
                {renderDetails()}
            </div>
    )
}

export default DetailsPresentationComponent;
