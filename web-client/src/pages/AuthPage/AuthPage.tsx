import React, {useState} from "react";
import {useCreateUserMutation, useGetCitiesQuery, useLoginMutation} from "../../generated/graphql";
import { RouteComponentProps, withRouter } from "react-router-dom";
import {setAccessToken} from "../../auth/accessToken";

interface Props extends RouteComponentProps{
    refetch: Function,
    strings: any,
}

const AuthPage: React.FC<Props> = (props) => {

        const cities = useGetCitiesQuery({
            variables: {
                countryCode: "RS"
            }
        })

        const [email, setEmail] = useState('');
        const [password, setPassword] = useState('');
        const [firstName, setFirstName] = useState('');
        const [lastName, setLastName] = useState('');
        const [phone, setPhone] = useState(0);
        const [city, setCity] = useState('');

        const [loginEmail, setLoginEmail] = useState('');
        const [loginPassword, setLoginPassword] = useState('');

        const [register] = useCreateUserMutation();
        const [login] = useLoginMutation();

        const {strings} = props;

        if (cities.loading) return <div>Loading...</div>
        if (cities.error) return <div>404</div>

        return (
            <div className="auth-page">
                <form className="auth-page-form" onSubmit={async e => {
                    e.preventDefault();
                    const response = await register({
                        variables: {
                            email,
                            password,
                            firstName,
                            lastName,
                            city,
                            phone
                        }
                    });
                    await props.refetch();
                    props.history.push("/");
                    console.log(response)
                }}>
                    <h1 className="auth-page-form__header">{strings.Register}</h1>

                        <div className="first-and-last-name-container">
                            <input className="first-and-last-name-container__input" type="text" value={firstName} placeholder="Ime" onChange={e => {
                                setFirstName(e.target.value)
                            }}/>

                            <input className="first-and-last-name-container__input" type="text" value={lastName} placeholder="Prezime" onChange={e => {
                                setLastName(e.target.value)
                            }}/>
                        </div>

                        <input className="auth-page-form__input" type="number" value={phone} placeholder="Telefon" onChange={e => {
                            e.preventDefault();
                            if((isFinite(parseInt(e.target.value)))) {
                                setPhone(parseInt(e.target.value))
                            }

                        }}/>

                        <select onSelect={(e: any) => {
                            e.preventDefault()
                            setCity(e.target.value)
                        }} name="cities" className="form-control required styled-select"
                                style={{width: '21vw', margin: 'auto', backgroundColor: '#f3f2f2'}} id="">
                            <option
                                    hidden
                            >Mesto</option>
                            {
                                cities?.data?.getCities.map((city: any) => {
                                    return <option key={city.id} value={city.name}>{city.name}</option>
                                })
                            }
                        </select>

                        <input className="auth-page-form__input" type="text" value={email} placeholder="email" onChange={e => {
                            setEmail(e.target.value)
                        }}/>

                        <input type="password" className="auth-page-form__input" value={password} placeholder={strings.Password} onChange={e => {
                            setPassword(e.target.value)
                        }}/>

                    <button className="auth-page-form__button" type="submit">{strings.Register}</button>
                </form>

                <form className="auth-page-form" onSubmit={async e => {
                    e.preventDefault();
                    const response = await login({
                        variables: {
                            loginEmail,
                            loginPassword
                        }
                    });

                    if (response && response.data && response.data.loginUser && response.data.loginUser.token) {
                        setAccessToken(response.data.loginUser.token);
                        localStorage.setItem('token', `Bearer ${response.data.loginUser.token}`)
                    }

                    await props.refetch();
                    props.history.push("/");
                    console.log(response)
                }}>
                    <h1 className="auth-page-form__header">{strings.Login}</h1>

                    <input className="auth-page-form__input" type="text" value={loginEmail} placeholder="email" onChange={e => {
                            setLoginEmail(e.target.value)
                        }}/>


                        <input className="auth-page-form__input" type="password" value={loginPassword} placeholder={strings.Password} onChange={e => {
                            setLoginPassword(e.target.value)
                        }}/>

                    <button className="auth-page-form__button" type="submit">{strings.Login}</button>
                </form>

                <div className="auth-page__terms-of-use">
                    <h1 className="auth-page__terms-of-use-header">{strings.TermsOfUse}</h1>
                    <p className="auth-page__terms-of-use-paragraph">
                        Lorem ipsum dolor sit amet consectetur adipisicing elit. Ab ullam, autem est molestiae explicabo molestias, at tenetur voluptas quam alias dolores eos provident aperiam facere magni ea voluptatibus beatae repellat.
                        Lorem ipsum dolor sit, amet consectetur adipisicing elit. Ipsum nulla ex corrupti atque molestias distinctio optio id quaerat at fugiat illo aperiam, esse eaque saepe cupiditate sunt. Quis, dicta vel!
                    </p>
                </div>
            </div>
        );
};

export default withRouter(AuthPage);