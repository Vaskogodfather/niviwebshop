import React from "react";

export enum InputTypes{
    TEXT = "TEXT",
    MULTILINE_TEXT = "MULTILINE_TEXT",
    NUMBER = "NUMBER",
    DROPDOWN_LIST = "DROPDOWN_LIST",
    CHECKBOX_LIST = "CHECKBOX_LIST",
    RADIO_LIST = "RADIO_LIST"
}

export interface ListingModel {
    inputName: string,
    inputValue: string,
    inputType: InputTypes,
    inputList?: Array<any>,
    required: boolean,
    searchable: boolean
}

let result: ListingModel[] = [];

export const generateFormFromJSON = (listingValues: any, setListingValues: any,step: number, json?: JSON) => {
    result = [];
    if(json) {
        for (let [_, value] of Object.entries(json)) {
            if (_) {
                const val = value;
                let m: ListingModel = {} as ListingModel;
                for (let [key] of Object.entries(val)) {
                    if (key.toString() === "name") m.inputName = val[key];
                    if (key.toString() === "value") m.inputValue = val[key];
                    if (key.toString() === "type") m.inputType = val[key];
                    if (key.toString() === "list") m.inputList = val[key];
                    if (key.toString() === "required") m.required = val[key];
                    if (key.toString() === "searchable") m.searchable = val[key];
                }
                result.push(m);
            }
        }
    }
    return populateInputForms(listingValues, setListingValues, step);
};

const populateInputForms = (listingValues: any, setListingValues: any, step: number) => {

    const form = result.map(({inputList, inputName, inputType}) => {

        if(inputType === InputTypes.NUMBER){
            return <div
                style={{display: `${step === 2 ? '' : 'none'}`}}
                key={inputName}>
                <h3>{inputName}</h3>
                <input
                    className="create-listing-text-input"
                    name={listingValues.Kubikaza}
                placeholder={inputName}
                type="number"
                value={listingValues[inputName]}
                onChange={(event => {
                    event.preventDefault();
                    setListingValues({
                        ...listingValues,
                        details: {
                            ...listingValues.details,
                            [inputName]: event.target.value
                        }
                    })
                })}
            /></div>
        }
        else if(inputType === InputTypes.TEXT){
            return <div
                style={{display: `${step === 2 ? '' : 'none'}`}}
                key={inputName}>
                <h3>{inputName}</h3>
                <input
                    className="create-listing-text-input"
                    key={inputName}
                name={inputName}
                value={listingValues[inputName]}
                placeholder={inputName}
                type="text"
                onChange={(event => {
                    event.preventDefault();
                    setListingValues({
                        ...listingValues,
                        details: {
                            ...listingValues.details,
                            [inputName]: event.target.value
                        }
                    })
                })}
            /></div>
        }
        else if(inputType === InputTypes.MULTILINE_TEXT){
            return <div
                style={{display: `${step === 2 ? '' : 'none'}`}}
                key={inputName}>
                <h3>{inputName}</h3>
                <textarea
                    className="create-listing-text-input"
                    cols={3}
                rows={3}
                key={inputName}
                name={inputName}
                value={listingValues[inputName]}
                placeholder={inputName}
                onChange={(event => {
                    event.preventDefault();
                    setListingValues({
                        ...listingValues,
                        details: {
                            ...listingValues.details,
                            [inputName]: event.target.value
                        }
                    })
                })}
            /></div>
        }
        else if (inputType === InputTypes.DROPDOWN_LIST){
            return <div
                style={{display: `${step === 2 ? '' : 'none'}`}}
                key={inputName}>
                <h3>{inputName}</h3>
                <select id="selectOptions" className="form-control required styled-select" key={inputName} value={listingValues[inputName]}>
                    <option value={listingValues.details[inputName]}
                            hidden
                    >{inputName}</option>
                {
                    inputList?.map(item => {
                        return <option
                            className="option-grams"
                            onClick={event => {
                                event.preventDefault();
                                setListingValues({
                                    ...listingValues,
                                    details: {
                                        ...listingValues.details,
                                        [inputName]: item ? item : ''
                                    }
                                })
                            }}
                            value={listingValues.details[inputName]} key={item}>{item}</option>
                    })
                }
            </select></div>
        }
        else if (inputType === InputTypes.CHECKBOX_LIST){
            let returnValues = {
                [inputName]: listingValues.details[inputName] ? listingValues.details[inputName] : []
            };
            return (
                <div
                    style={{display: `${step === 2 ? '' : 'none'}`}}
                    key={Math.random()}>
                <h3>{inputName}</h3>
                <ul className="checkbox-list-create-listing">
                {
                    inputList?.map(item => {
                        return (<li key={item}>
                            <input key={item} onChange={(event) => {
                                event.preventDefault();
                                returnValues[inputName].includes(item) ?
                                    returnValues[inputName].splice(returnValues[inputName].indexOf(item), 1)
                                    : returnValues[inputName].push(item);
                                setListingValues({
                                    ...listingValues,
                                    details: {
                                        ...listingValues.details,
                                        [inputName]: returnValues[inputName]
                                    }
                                });
                            }} type="checkbox"
                                   value={item}
                                   checked={listingValues.details[inputName] ? listingValues.details[inputName].includes(item) : false}
                                    // onChange={e => {
                                    //     e.preventDefault()
                                    // }}
                            /> {item}</li>)
                    })
                }
            </ul></div>)
        }
        else {
            return null
        }
    });
    return form.filter(o=>!!o);
};