import { PubSub } from "apollo-server-express";
import {Redis} from "ioredis";

export interface Context {
    pubsub: PubSub;
    request: any;
    redis: Redis;
    url: string;
}

export type Resolver = (
    parent: any,
    args: any,
    context: Context,
    info: any
) => any;

export interface ResolverMap {
    [key: string]: {
        [key: string]: Resolver | { [key: string]: Resolver };
    };
}

