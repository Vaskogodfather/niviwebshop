import {User} from "../../entity/User";
import {Request, Response} from "express";
import {redis} from "../redis";

export const confirmEmail = async (req: Request, res: Response) => {
    const { id } = req.params;
    const userId = await redis.get(id);
    if (userId == null) {
        res.send("invalid link");
    } else {
        await User.update({_id: userId}, {emailConfirmed: true});
        await redis.del(id);
        // TODO - redirect to frontend
        res.send("ok");
    }
};