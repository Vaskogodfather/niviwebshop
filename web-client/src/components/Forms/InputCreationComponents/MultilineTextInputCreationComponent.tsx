import { useState } from "react";
import {InputModel} from "../../../entities/InputModel";
import {InputTypes} from "../../../entities/InputTypes";

interface Props {
    addToInputs: any,
    removeFromInputs: any
}
const MultilineTextInputCreationComponent: React.FC<Props> = (props) => {

    const [inputName, setInputName] = useState('');
    const [inputValue, setInputValue] = useState('');
    const [required, setRequired] = useState(false);
    const [searchable, setSearchable] = useState(false);

    const createInput = () => {
        let input: InputModel = new InputModel(inputName, inputValue, InputTypes.MULTILINE_TEXT, required, searchable, undefined);
        props.addToInputs(input);
    };

    const removeInput = () => {
        let input: InputModel = new InputModel(inputName, inputValue, InputTypes.MULTILINE_TEXT, required, searchable, undefined);
        props.removeFromInputs(input);
    };


    return (
        <div className="inputsCreationCard">
            <p className="inputsCreationCard__item">Multiline Text input</p>
            <input value={inputName} onChange={e => {
                setInputName(e.target.value)
                setInputValue(e.target.value)
            }} className="inputsCreationCard__item" type="text" placeholder="input name"/>

            <div className="inputsCreationCard__item">
                <input checked={required} onChange={e => setRequired(!required)} type="checkbox" name="required"/> Required
            </div>

            <div className="inputsCreationCard__item">
                <input checked={searchable} onChange={e => setSearchable(!searchable)} type="checkbox" name="required"/> Searchable
            </div>

            <button className="inputsCreationCard__item" onClick={e => {
                createInput()
            }}>Confirm input</button>

            <button className="inputsCreationCard__item" onClick={e => {
                removeInput()
            }}>Remove input</button>

        </div>
    );
};

export default MultilineTextInputCreationComponent;