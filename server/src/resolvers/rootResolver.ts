import _ = require('lodash');
import {userResolvers} from "./userResolvers";
import {categoryResolvers} from "./categoryResolvers";
import {subcategoryResolvers} from "./subcategoryResolvers";
import {inputsResolvers} from "./inputsResolvers";
import {listingResolvers} from "./listingResolvers";

const merged = _.merge(
    userResolvers,
    categoryResolvers,
    subcategoryResolvers,
    listingResolvers,
    inputsResolvers,

);

const resolvers = {
    ...merged
};

export {resolvers};