import {TypePolicies} from "@apollo/client/cache/inmemory/policies";

export const customTypePolicies: TypePolicies = {
    Category: {
      fields: {
          subcategories: {
              merge(existing, incoming) {
                  return incoming;
              }
          },
          listings: {
              merge(existing, incoming) {
                  return incoming;
              }
          }
      }
    },
    Subcategory: {
        fields: {
            category: {
                merge(existing, incoming) {
                    return incoming;
                }
            },
            listings: {
                merge(existing, incoming) {
                    return incoming;
                }
            }
        }
    },
    Listing: {
      fields: {
          category: {
              merge(existing, incoming) {
                  return incoming;
              }
          },
          subcategory: {
              merge(existing, incoming) {
                  return incoming;
              }
          },
      }
    },
    Query: {
        fields: {
            categories: {
                merge(existing, incoming) {
                    return incoming;
                },
            },
            subcategories: {
                merge(existing, incoming) {
                    return incoming;
                },
            },
            listings: {
                merge(existing, incoming) {
                    return incoming;
                }
            }
        },
    },
};