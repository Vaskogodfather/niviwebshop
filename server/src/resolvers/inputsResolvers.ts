import {ResolverMap} from "../utils/resolver-map";
// import {resolveCreateListingInputs} from "../models/inputsHandler";
import {Category} from "../entity/Category";
import {Subcategory} from "../entity/Subcategory";
const cities = require('all-the-cities');

const inputsResolvers: ResolverMap = {
    Query: {
        "getAddListingInputs": async (_, {categoryInputId, subcategoryInputId}: GQL.IGetAddListingInputsOnQueryArguments) => {
            const inputs: string[] = [categoryInputId ? categoryInputId : '', subcategoryInputId ? subcategoryInputId : ''];
            console.log(inputs.length);
            const category = await Category.findOne({where: {inputsIdentifier: categoryInputId}});
            const subcategory = await Subcategory.findOne({where: {inputsIdentifier: subcategoryInputId}});

            // @ts-ignore
            const categoryInputsRs = await JSON.parse(category?.inputs);
            // @ts-ignore
            const subcategoryInputsRes = await JSON.parse(subcategory?.inputs);
            const allInputs = await categoryInputsRs.concat(subcategoryInputsRes)

            return allInputs || null
        },
        "getCities": async (_, {countryCode}: GQL.IGetCitiesOnQueryArguments) => {
            const cityList = cities.filter((city: any) => city.country.match(countryCode))
            return cityList.reverse();
        }
    }
};

export {inputsResolvers}