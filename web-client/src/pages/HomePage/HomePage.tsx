import React from "react";
import {Query, useCategoriesQuery} from "../../generated/graphql";
import {Link} from "react-router-dom";

interface Props {
    refetch: Function,
    session: Query
}

export const HomePage: React.FC<Props> = (Props) => {

    const {data, loading} = useCategoriesQuery({
        fetchPolicy: 'network-only'
    });
    if (loading || !data || !data.categories) return <div>Loading...</div>

    return (
        <div className="home-page">
            <div className="home-page__categories">
                {
                    data.categories.map(category => (
                        <Link key={category._id} to={`/category/${category._id}`} className="category-card">
                            <img src={`http://localhost:1111/images/${category.categoryImage}`} alt="logo"
                                 className="category-card__image"/>
                            <p className="category-card__header">{category.categoryName}</p>
                        </Link>
                    ))
                }
            </div>
        </div>
    );
};