import React from 'react';
import {CategoriesDocument, Query, useCategoriesQuery, useDeleteCategoryMutation} from "../../../generated/graphql";
import {Link} from "react-router-dom";

interface Props {
    session: Query
}

export const CategoriesAdminPage: React.FC<Props> = (props) => {

    let {data: categories} = useCategoriesQuery({fetchPolicy: 'network-only'});
    const [deleteCategory] = useDeleteCategoryMutation();

    return(
        <div className="admin-container">
            <h1 className="category-card__header">Categories</h1>
            <Link to="/create-category" className="category-item-admin-button--change" style={{margin: 'auto', textAlign: 'center'}}>Create category</Link>
            {
                categories?.categories?.map(category => {
                    return (
                        <div key={category._id} className="category-item-admin-container">
                            <p className="category-item-admin-paragraph"><strong>Category: </strong>{category.categoryName}</p>
                                <img src={`http://localhost:1111/images/${category.categoryImage}`} alt="logo" className="category-card__image"/>
                            <div></div>

                            <Link to={`/edit-category/${category._id}`} className="category-item-admin-button--change">Uredi kategoriju</Link>

                            <button
                                className="category-item-admin-button--delete"
                                onClick={async event => {
                                    event.preventDefault();
                                    event.stopPropagation();
                                    await deleteCategory({
                                        variables: {
                                            id: category._id
                                        },
                                        refetchQueries: [{query: CategoriesDocument}]
                                    });
                                }}
                            >Izbrisi kategoriju</button>
                        </div>
                    )
                })
            }

        </div>
    )

};