import React from 'react';
import {Link} from "react-router-dom";

interface Props {
    refetch: any,
    session: any
}

export const AdminPanel: React.FC<Props> = (Props) => {

    return(
        <div>
            <h1>Admin Panel</h1>
            <Link to="/admin-categories">Categories</Link>
        </div>
    )

};