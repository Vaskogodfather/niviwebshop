import {InputTypes} from "../entities/InputTypes";

export interface ListingModel {
    inputName: string,
    inputValue: string,
    inputType: InputTypes,
    inputList?: Array<any>,
    required: boolean,
    searchable: boolean
}

let result: ListingModel[] = [];
let json: JSON;

export const generateSearchFormFromJSON = (listingValues: any, setListingValues: any, jsonArr?: any[]) => {
    console.log(jsonArr)
    if (jsonArr) {
        json = JSON.parse(JSON.stringify(jsonArr));
    }
    result = [];
    if(json) {
        for (let [_, value] of Object.entries(json)) {
            if (_) {
                for (let [, inputsArr] of Object.entries(value)) {
                    if (typeof inputsArr === 'object' && inputsArr !== null) {
                        // @ts-ignore
                        JSON.parse(inputsArr.inputs).map((res) => {
                            let m: ListingModel = {} as ListingModel;
                            m.inputName = res.name;
                            m.inputValue = res.value;
                            m.inputType = res.type;
                            m.inputList = res.list;
                            m.required = res.required;
                            m.searchable = res.searchable;

                            if (!result.includes(res.name)) {
                                result.push(m);
                                return
                            }
                        })
                    }
                }
            }
        }
    }

    let names = result.map(o => o.inputName);
    result = result.filter(({inputName}, index) => !names.includes(inputName, index + 1));
    return populateSearchInputsForm(listingValues, setListingValues);
};

const populateSearchInputsForm = (listingValues: any, setListingValues: any) => {
    const form = result.map(({inputList, inputName, inputType}) => {
        if (inputType === InputTypes.DROPDOWN_LIST){
            return <div
                key={inputName}>
                <h3>{inputName}</h3>
                <select id="selectOptions" className="form-control required styled-select" key={inputName} value={listingValues[inputName]}>
                    <option value={listingValues.details[inputName]}
                            hidden
                    >{inputName}</option>
                    {
                        inputList?.map(item => {
                            return <option
                                className="option-grams"
                                onClick={event => {
                                    event.preventDefault();
                                    setListingValues({
                                        ...listingValues,
                                        details: {
                                            ...listingValues.details,
                                            [inputName]: item ? item : ''
                                        }
                                    })
                                }}
                                value={listingValues.details[inputName]} key={item}>{item}</option>
                        })
                    }
                </select></div>
        }
    });
    return form.filter(o=>!!o);
};