import React, {useEffect, useMemo, useState} from "react";
import {useDropzone} from "react-dropzone";
import {Query, useCreateSubcategoryMutation} from "../../../generated/graphql";
import {acceptStyle, activeStyle, baseStyle, rejectStyle} from "../../../styles/imageUploadDialogStyle";
import {RouteComponentProps, withRouter} from "react-router-dom";
import {InputTypes} from "../../../entities/InputTypes";
import {createNewInput, returnAllInputs} from "../../../handlers/InputsCreationHandler";

interface Props extends RouteComponentProps<{id: string}>{
    session: Query
}

const AddNewSubcategoryPage: React.FC<Props> = (props) => {

    const [files, setFiles] = useState([]);
    const [subcategoryName, setSubcategoryName] = useState('');
    const [createSubcategory] = useCreateSubcategoryMutation();

    const [selectedInputType, setSelectedInputType] = useState(InputTypes.TEXT);
    const [newInput, setNewInput] = useState([]);
    let inputTypes: any[] = [];

    const {acceptedFiles, fileRejections, getRootProps, getInputProps, isDragAccept, isDragReject, isDragActive} = useDropzone({
        accept: 'image/*',
        multiple: false,
        onDrop: acceptedFiles => {
            // @ts-ignore
            setFiles(acceptedFiles.map(file => Object.assign(file, {
                preview: URL.createObjectURL(file)
            })));
        }
    });

    const acceptedFileItems = acceptedFiles.map((file: any) => (
        <div className="thumb" key={file.name}>
            <div className="thumbInner">
                <img className="upload-image-preview" src={file.preview} alt=""/>
            </div>
        </div>
    ));

    const style = useMemo(() => ({
        ...baseStyle,
        ...(isDragActive ? activeStyle : {}),
        ...(isDragAccept ? acceptStyle : {}),
        ...(isDragReject ? rejectStyle : {})
    }), [isDragActive, isDragReject, isDragAccept]);

    useEffect(() => () => {
        // Make sure to revoke the data uris to avoid memory leaks
        // @ts-ignore
        files.forEach(file => URL.revokeObjectURL(file.preview));
    }, [files]);

    const getInputTypes = () => {
        for (let inputType in InputTypes) {
            if (isNaN(Number(inputType))) {
                inputTypes.push(inputType)
            }
        }
        return
    };

    return (
        <div className="admin-container">
            <input type="text"
                   className="auth-page-form__input"
                   value={subcategoryName} placeholder="Ime podkategorije" onChange={e => {
                setSubcategoryName(e.target.value)
            }}
            />
            <section>
                <div {...getRootProps(
                    //@ts-ignore
                    {style}
                )}>
                    <input {...getInputProps()} />
                    {isDragAccept && (<p>All files will be accepted</p>)}
                    {isDragReject && (<p>Some files will be rejected</p>)}
                    {!isDragActive && (<p>Dodaj novu sliku ...</p>)}
                </div>
                <aside className="thumb-container">
                    <ul>{acceptedFileItems}</ul>
                </aside>
            </section>

            <div>
                <select id="selectOptions" className="form-control required styled-select">
                    {getInputTypes()}
                    {
                        inputTypes.map(inputType => {
                            return (<option
                                onClick={_ => setSelectedInputType(inputType)}
                                value={selectedInputType}>{inputType}</option>)
                        })
                    }
                </select>

                <button onClick={e => {
                    e.preventDefault()
                    let input: any[] = [...newInput, createNewInput(selectedInputType)];
                    // @ts-ignore
                    setNewInput(input)
                }}>Add input
                </button>

                {newInput}
            </div>


            <button className="auth-page-form__button" onClick={async e => {
                e.preventDefault();
                console.log('called')
                // console.log(await JSON.stringify(returnAllInputs()))
                if (acceptedFiles.length > 0 && fileRejections.length === 0 && subcategoryName.length > 0 && props.match.params.id) {
                     await createSubcategory({
                        variables: {
                            id: props.match.params.id,
                            subcategoryName: subcategoryName,
                            image: acceptedFiles[0],
                            inputs: await JSON.stringify(await returnAllInputs())
                        }

                    });
                    props.history.push(`/edit-category/${props.match.params.id}`);
                }
            }}>Dodaj podkategoriju</button>
        </div>
    );

};

export default withRouter(AddNewSubcategoryPage);