import {ResolverMap} from "../utils/resolver-map";
import {Subcategory} from "../entity/Subcategory";
import {Category} from "../entity/Category";
import {processDelete, processUpload} from "../utils/FilesHandlers/localFileHandler";

const subcategoryResolvers: ResolverMap = {
    Query: {
        subcategories: async (_, {categoryId}: GQL.ISubcategoriesOnQueryArguments) => {
            const subcategories = await Subcategory.find({
                where: categoryId ? {category: categoryId} : "",
                relations: ["category", "listings", "listings.owner"]
            });

            if(!subcategories) return null;

            return subcategories;
        }
    },
    Mutation: {
        "createSubcategory": async (_, {data}: GQL.ICreateSubcategoryOnMutationArguments) => {

            const {categoryId,subcategoryName, image, inputs} = data;
            if(!categoryId || !subcategoryName || !image) throw new Error('Invalid Arguments');
            console.log(inputs)
            const category = await Category.findOne({where: {_id: categoryId}});
            if(category) {
                const pictureUrl = await processUpload(image, "subcategories");

                const newSubcategory = await new Subcategory();
                newSubcategory.subcategoryName = subcategoryName;
                newSubcategory.subcategoryImage = pictureUrl;
                newSubcategory.category = category;
                newSubcategory.inputsIdentifier = subcategoryName + "_inputs"
                newSubcategory.inputs = inputs || "";

                await newSubcategory.save();
                return newSubcategory;
            } else {
                throw new Error('No Category found')
            }
        },
        "deleteSubcategory": async (_, {subcategoryId}: GQL.IDeleteSubcategoryOnMutationArguments) => {
            const subcategory = await Subcategory.findOne({where: {_id: subcategoryId}, relations: ["category"]});
            if(!subcategory) return false;
            await processDelete(subcategory.subcategoryImage);
            await subcategory?.remove();
            return true;
        }
    }
};

export {subcategoryResolvers}