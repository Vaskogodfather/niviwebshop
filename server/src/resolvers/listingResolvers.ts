import {ResolverMap} from "../utils/resolver-map";
import {Listing} from "../entity/Listing";
import {User} from "../entity/User";
import {Category} from "../entity/Category";
import {Subcategory} from "../entity/Subcategory";
import {processUpload} from "../utils/FilesHandlers/localFileHandler";
import {getConnection} from "typeorm";

const listingResolvers: ResolverMap = {
    Query: {
        listings: async (_, {data}: GQL.IListingsOnQueryArguments, {}) => {
            const {listingId, categoryId, subcategoryId, name, details} = data;

            let listings = await getConnection()
                .getRepository(Listing)
                .createQueryBuilder("search")
                .leftJoinAndSelect("search.category", "category")
                .leftJoinAndSelect("search.subcategory", "subcategory")
                .leftJoinAndSelect("search.owner", "owner");
            if (listingId) {
                listings = listings.andWhere("search._id = :listingId", {listingId})
            }
            if (categoryId) {
                listings = listings.andWhere("search.category = :categoryId", {categoryId})
            }
            if (subcategoryId) {
                listings = listings.andWhere("search.subcategory = :subcategoryId", {subcategoryId})
            }
            if (name) {
                listings = listings.andWhere("search.name like :name", {name: `%${name}%`})
            }

            if(details){
                console.log(JSON.parse(details));
            }

            if (!listings) return null;
            return listings.getMany();
        }
    },
  Mutation: {
      "createListing": async (_, {data}: GQL.ICreateListingOnMutationArguments, {request}) => {

          if(!request.req.isAuth) {
              throw new Error('Unauthorized')
          }

          const {name, details, price, currency, description, category, subcategory, images} = data;

          const user = await User.findOne({where: {_id: request.req.userId}});
          const categoryRes = await Category.findOne({where: {_id: category}});
          const subcategoryRes = await Subcategory.findOne({where: {_id: subcategory}});
          if(!user) throw new Error('No user');
          if(!categoryRes) throw new Error('No category');
          if(!subcategoryRes) throw new Error('No subcategory');

          const pictureUrls = await Promise.all(images ? images.map(image => processUpload(image, "listings")) : '');

          const urls = await pictureUrls.join(',');
          const newListing = await new Listing();

              newListing.name = name;
              newListing.price = price;
              newListing.currency = currency;
              newListing.description = description ? description : '';
              newListing.images = urls;
              newListing.details = JSON.parse(details);
              newListing.owner = user;
              newListing.category = categoryRes;
              newListing.subcategory = subcategoryRes;

              await newListing.save();

             return await Listing.findOne({where: {_id: newListing._id},
              relations: ["category", "subcategory", "owner"]
          })
      }
  }
};

export {listingResolvers}