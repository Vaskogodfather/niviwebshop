import React, {Fragment} from 'react';
import {Link, RouteComponentProps, withRouter} from "react-router-dom";
import {Query} from "../../generated/graphql";
import HeaderLogo from "./HeaderLogo";
import DefaultUserLogo from '../../images/header/defaultuser.png'

interface Props extends RouteComponentProps{
    session: Query,
    refetch: Function,
    strings: any
}

const HeaderComponent: React.FC<Props> =(props) => {

    const {user} = props?.session?.me;
    const {strings} = props;

    const GuestLink = () => (
        <Fragment>
            <div className="header-user-nav__user">
                <Link to="/auth" className="header-user-nav__user-name">{strings.Authentication}</Link>
            </div>
        </Fragment>
    );

    const AuthLinks = () => (
        <Fragment>
            <div className="header-user-nav__icon-box">
                <svg className="header-user-nav__icon">
                    <use xlinkHref="/images/svg-icons/sprite.svg#icon-newspaper" />
                </svg>
            </div>

            <div className="header-user-nav__icon-box">
                <svg className="header-user-nav__icon">
                    <use xlinkHref="/images/svg-icons/sprite.svg#icon-group" />
                </svg>
            </div>

            <div className="header-user-nav__icon-box">
                <svg className="header-user-nav__icon">
                    <use xlinkHref="/images/svg-icons/sprite.svg#icon-chat" />
                </svg>
                <span className="header-user-nav__notification">13</span>
            </div>

            <div className="header-user-nav__user">
                <img src={DefaultUserLogo} alt="logo" className="header-user-nav__user-photo"/>
                <Link to={user ? "/dashboard" : "/create-profile"} className="header-user-nav">{user?.email.split('@')[0]}</Link>
            </div>
        </Fragment>
    );

    let body: JSX.Element | null;

    if (!props.session?.me) { body = null }
    else if (user) { body = <AuthLinks/> }
    else { body = <GuestLink/> }

        return (
            <header className="header">
                <h1 className="niviem-logo"><HeaderLogo copy="NiViEm shop" role="heading" /></h1>
                <form action="#" className="header-search-bar">
                    <input className="header-search-bar__input" type="text" placeholder={strings.HeaderAddSearch}/>
                    <button className="header-search-bar__button">
                        <svg className="header-search-bar__icon">
                            <use xlinkHref="/images/svg-icons/sprite.svg#icon-magnifying-glass" />
                        </svg>
                    </button>
                </form>

                <nav className="header-user-nav">
                    {body}
                </nav>
            </header>
        );
};

export default withRouter(HeaderComponent);