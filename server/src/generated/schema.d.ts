// tslint:disable
// graphql typescript definitions

declare namespace GQL {
interface IGraphQLResponseRoot {
data?: IQuery | IMutation | ISubscription;
errors?: Array<IGraphQLResponseError>;
}

interface IGraphQLResponseError {
/** Required for all errors */
message: string;
locations?: Array<IGraphQLResponseErrorLocation>;
/** 7.2.2 says 'GraphQL servers may provide additional entries to error' */
[propName: string]: any;
}

interface IGraphQLResponseErrorLocation {
line: number;
column: number;
}

interface IQuery {
__typename: "Query";
test: string;
me: IUserPayload;
user: IUserPayload;
users: Array<IUsersPayload> | null;
categories: Array<ICategory> | null;
subcategories: Array<ISubcategory> | null;
listings: Array<IListing> | null;
getAddListingInputs: any | null;
getCities: any | null;
}

interface IUserOnQueryArguments {
_id: string;
}

interface ICategoriesOnQueryArguments {
categoryId?: string | null;
}

interface ISubcategoriesOnQueryArguments {
categoryId?: string | null;
}

interface IListingsOnQueryArguments {
data: ISearchListingsInput;
}

interface IGetAddListingInputsOnQueryArguments {
categoryInputId?: string | null;
subcategoryInputId?: string | null;
}

interface IGetCitiesOnQueryArguments {
countryCode: string;
}

interface IMutation {
__typename: "Mutation";
loginUser: IAuthPayload;
createUser: IAuthPayload;
sendForgotPasswordEmail: boolean | null;
changeForgottenPassword: IUserPayload;
updateOnlineStatus: IUserPayload;
createCategory: ICategory;
updateCategory: ICategory;
deleteCategory: boolean;
createSubcategory: ISubcategory;
deleteSubcategory: boolean;
createListing: IListing;
}

interface ILoginUserOnMutationArguments {
email: string;
password: string;
}

interface ICreateUserOnMutationArguments {
data: ICreateUserInput;
}

interface ISendForgotPasswordEmailOnMutationArguments {
email: string;
}

interface IChangeForgottenPasswordOnMutationArguments {
newPassword: string;
key: string;
}

interface ICreateCategoryOnMutationArguments {
data: ICreateCategoryInput;
}

interface IUpdateCategoryOnMutationArguments {
data: IUpdateCategoryInput;
}

interface IDeleteCategoryOnMutationArguments {
categoryId: string;
}

interface ICreateSubcategoryOnMutationArguments {
data: ICreateSubcategoryInput;
}

interface IDeleteSubcategoryOnMutationArguments {
subcategoryId: string;
}

interface ICreateListingOnMutationArguments {
data: ICreateListingInput;
}

interface ISubscription {
__typename: "Subscription";
onlineStatus: IUser | null;
hello: string | null;
}

interface ICreateUserInput {
email: string;
password: string;
firstName: string;
lastName: string;
phone: number;
city: string;
}

interface ICreateCategoryInput {
categoryName: string;
image: any;
inputs?: any | null;
}

interface IUpdateCategoryInput {
categoryId: string;
newCategoryName?: string | null;
newCategoryImage?: string | null;
inputs?: any | null;
}

interface ICreateSubcategoryInput {
categoryId: string;
subcategoryName: string;
image: any;
inputs?: any | null;
}

interface IUpdateSubcategoryInput {
subcategoryId: string;
subcategoryName?: string | null;
image?: any | null;
categoryId?: string | null;
inputs?: any | null;
}

interface ICreateListingInput {
name: string;
description?: string | null;
price: number;
currency: string;
images?: Array<any> | null;
details?: any | null;
subcategory: string;
category: string;
}

interface ISearchListingsInput {
listingId?: string | null;
categoryId?: string | null;
subcategoryId?: string | null;
name?: string | null;
details?: any | null;
}

interface ICategory {
__typename: "Category";
_id: string;
categoryName: string;
categoryImage: string;
inputsIdentifier: string | null;
inputs: any | null;
subcategories: Array<ISubcategory> | null;
listings: Array<IListing> | null;
}

interface ISubcategory {
__typename: "Subcategory";
_id: string;
subcategoryName: string;
subcategoryImage: string;
inputsIdentifier: string | null;
inputs: any | null;
category: ICategory;
listings: Array<IListing> | null;
}

interface IListing {
__typename: "Listing";
_id: string;
name: string;
description: string | null;
price: number;
currency: string;
images: string | null;
details: any | null;
owner: IUser;
subcategory: ISubcategory;
category: ICategory;
}

interface IUser {
__typename: "User";
_id: string;
email: string;
firstName: string;
lastName: string;
phone: number;
city: string;
last_seen: string;
listings: Array<IListing> | null;
}

interface IUserCreatedPayload {
__typename: "UserCreatedPayload";
mutation: MutationType;
data: IUser;
}

interface IUserPayload {
__typename: "UserPayload";
user: IUser | null;
errors: Array<IError> | null;
}

interface IUsersPayload {
__typename: "UsersPayload";
users: Array<IUser> | null;
errors: Array<IError> | null;
}

interface IAuthPayload {
__typename: "AuthPayload";
user: IUser | null;
token: string | null;
errors: Array<IError> | null;
}

interface IError {
__typename: "Error";
path: string;
message: string;
}

const enum MutationType {
CREATED = 'CREATED',
UPDATED = 'UPDATED',
DELETED = 'DELETED'
}
}

// tslint:enable
