import React from "react";
import {Link, RouteComponentProps} from "react-router-dom";
import {useSubcategoriesQuery} from "../../generated/graphql";
import {ListingsSidebar} from "../ListingsPage/ListingsSidebar";

interface Props extends RouteComponentProps<{id: string}>{}

export const CategoryPage: React.FC<Props> = ({match}) => {

    const {loading, data} = useSubcategoriesQuery({
        fetchPolicy: 'network-only',
        variables: {
            categoryId: match.params.id
        }
    });

    if (loading) return <div>404</div>;
    if (!data || !data.subcategories) return <div>404</div>;

    return (
        <div className="ads-results-container">
        <div>
            {data?.subcategories[0]?.category?.categoryName ? <div className="subcategories-header">{data?.subcategories[0]?.category?.categoryName}</div> : null}
            <div className="home-page__categories">
                {
                    data.subcategories.map(subcategory => (
                        <Link key={subcategory._id} to={`/listings/${subcategory._id}`} className="category-card">
                            <img src={`http://localhost:1111/images/${subcategory.subcategoryImage}`} alt="logo"
                                 className="category-card__image"/>
                            <p className="category-card__header">{subcategory.subcategoryName}</p>
                        </Link>
                    ))
                }
            </div>
        </div>
            <ListingsSidebar subcategories={data?.subcategories}/>
        </div>
    )
};