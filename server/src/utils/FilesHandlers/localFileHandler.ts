import ErrnoException = NodeJS.ErrnoException;

const shortid = require('shortid');
import { createWriteStream, unlink } from "fs";

const storeUpload = async ({ createReadStream, mimetype }: any, savePath: string): Promise<any> => {
    // aseq2
    const extension = mimetype.split("/")[1];
    const id = `${savePath}/${shortid.generate()}.${extension}`;
    const path = `src/images/${id}`;
    const stream = createReadStream();
    return new Promise((resolve, reject) =>
        stream
            .pipe(createWriteStream(path))
            .on("finish", () => resolve({ id, path }))
            .on("error", reject)
    );
};

export const processUpload = async (upload: any, savePath: string) => {
    const { createReadStream, mimetype } = await upload;
    const { id } = await storeUpload({ createReadStream, mimetype }, savePath);
    return id;
};

export const processDelete = async (filename: string) => {
    await unlink(`src/images/${filename}`, (err: ErrnoException | null) => {
        if(err) {
            console.log(err)
        } else {
            console.log('file deleted')
        }
    })
};