export const CarBody: Array<string> = [
    'hečbeg',
    'limuzina',
    'karavan',
    'kabriolet'
];

export const FuelTypes: Array<string> = [
    'Benzin',
    'Dizel',
    'Metan CNG'
];

export const AdditionalEquipment: Array<string> = [
    'Airbag',
    'Kozna sedista'
]