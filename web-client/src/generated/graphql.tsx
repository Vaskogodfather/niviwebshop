import { gql } from '@apollo/client';
import * as Apollo from '@apollo/client';
export type Maybe<T> = T | null;
export type Exact<T extends { [key: string]: unknown }> = { [K in keyof T]: T[K] };
export type MakeOptional<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]?: Maybe<T[SubKey]> };
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]: Maybe<T[SubKey]> };
const defaultOptions =  {}
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
  FileUpload: any;
  JSON: any;
  /** The `Upload` scalar type represents a file upload. */
  Upload: any;
};


export type AuthPayload = {
  __typename?: 'AuthPayload';
  user?: Maybe<User>;
  token?: Maybe<Scalars['String']>;
  errors?: Maybe<Array<Error>>;
};

export enum CacheControlScope {
  Public = 'PUBLIC',
  Private = 'PRIVATE'
}

export type Category = {
  __typename?: 'Category';
  _id: Scalars['ID'];
  categoryName: Scalars['String'];
  categoryImage: Scalars['String'];
  inputsIdentifier?: Maybe<Scalars['String']>;
  inputs?: Maybe<Scalars['JSON']>;
  subcategories?: Maybe<Array<Subcategory>>;
  listings?: Maybe<Array<Listing>>;
};

export type CreateCategoryInput = {
  categoryName: Scalars['String'];
  image: Scalars['FileUpload'];
  inputs?: Maybe<Scalars['JSON']>;
};

export type CreateListingInput = {
  name: Scalars['String'];
  description?: Maybe<Scalars['String']>;
  price: Scalars['Int'];
  currency: Scalars['String'];
  images?: Maybe<Array<Scalars['FileUpload']>>;
  details?: Maybe<Scalars['JSON']>;
  subcategory: Scalars['String'];
  category: Scalars['String'];
};

export type CreateSubcategoryInput = {
  categoryId: Scalars['String'];
  subcategoryName: Scalars['String'];
  image: Scalars['FileUpload'];
  inputs?: Maybe<Scalars['JSON']>;
};

export type CreateUserInput = {
  email: Scalars['String'];
  password: Scalars['String'];
  firstName: Scalars['String'];
  lastName: Scalars['String'];
  phone: Scalars['Int'];
  city: Scalars['String'];
};

export type Error = {
  __typename?: 'Error';
  path: Scalars['String'];
  message: Scalars['String'];
};



export type Listing = {
  __typename?: 'Listing';
  _id: Scalars['ID'];
  name: Scalars['String'];
  description?: Maybe<Scalars['String']>;
  price: Scalars['Int'];
  currency: Scalars['String'];
  images?: Maybe<Scalars['String']>;
  details?: Maybe<Scalars['JSON']>;
  owner: User;
  subcategory: Subcategory;
  category: Category;
};

export type Mutation = {
  __typename?: 'Mutation';
  loginUser: AuthPayload;
  createUser: AuthPayload;
  sendForgotPasswordEmail?: Maybe<Scalars['Boolean']>;
  changeForgottenPassword: UserPayload;
  updateOnlineStatus: UserPayload;
  createCategory: Category;
  updateCategory: Category;
  deleteCategory: Scalars['Boolean'];
  createSubcategory: Subcategory;
  deleteSubcategory: Scalars['Boolean'];
  createListing: Listing;
};


export type MutationLoginUserArgs = {
  email: Scalars['String'];
  password: Scalars['String'];
};


export type MutationCreateUserArgs = {
  data: CreateUserInput;
};


export type MutationSendForgotPasswordEmailArgs = {
  email: Scalars['String'];
};


export type MutationChangeForgottenPasswordArgs = {
  newPassword: Scalars['String'];
  key: Scalars['String'];
};


export type MutationCreateCategoryArgs = {
  data: CreateCategoryInput;
};


export type MutationUpdateCategoryArgs = {
  data: UpdateCategoryInput;
};


export type MutationDeleteCategoryArgs = {
  categoryId: Scalars['String'];
};


export type MutationCreateSubcategoryArgs = {
  data: CreateSubcategoryInput;
};


export type MutationDeleteSubcategoryArgs = {
  subcategoryId: Scalars['String'];
};


export type MutationCreateListingArgs = {
  data: CreateListingInput;
};

export enum MutationType {
  Created = 'CREATED',
  Updated = 'UPDATED',
  Deleted = 'DELETED'
}

export type Query = {
  __typename?: 'Query';
  test: Scalars['String'];
  me: UserPayload;
  user: UserPayload;
  users?: Maybe<Array<UsersPayload>>;
  categories?: Maybe<Array<Category>>;
  subcategories?: Maybe<Array<Subcategory>>;
  listings?: Maybe<Array<Listing>>;
  getAddListingInputs?: Maybe<Scalars['JSON']>;
  getCities?: Maybe<Scalars['JSON']>;
};


export type QueryUserArgs = {
  _id: Scalars['String'];
};


export type QueryCategoriesArgs = {
  categoryId?: Maybe<Scalars['String']>;
};


export type QuerySubcategoriesArgs = {
  categoryId?: Maybe<Scalars['String']>;
};


export type QueryListingsArgs = {
  data: SearchListingsInput;
};


export type QueryGetAddListingInputsArgs = {
  categoryInputId?: Maybe<Scalars['String']>;
  subcategoryInputId?: Maybe<Scalars['String']>;
};


export type QueryGetCitiesArgs = {
  countryCode: Scalars['String'];
};

export type SearchListingsInput = {
  listingId?: Maybe<Scalars['String']>;
  categoryId?: Maybe<Scalars['String']>;
  subcategoryId?: Maybe<Scalars['String']>;
  name?: Maybe<Scalars['String']>;
  details?: Maybe<Scalars['JSON']>;
};

export type Subcategory = {
  __typename?: 'Subcategory';
  _id: Scalars['String'];
  subcategoryName: Scalars['String'];
  subcategoryImage: Scalars['String'];
  inputsIdentifier?: Maybe<Scalars['String']>;
  inputs?: Maybe<Scalars['JSON']>;
  category: Category;
  listings?: Maybe<Array<Listing>>;
};

export type Subscription = {
  __typename?: 'Subscription';
  onlineStatus?: Maybe<User>;
  hello?: Maybe<Scalars['String']>;
};

export type UpdateCategoryInput = {
  categoryId: Scalars['String'];
  newCategoryName?: Maybe<Scalars['String']>;
  newCategoryImage?: Maybe<Scalars['String']>;
  inputs?: Maybe<Scalars['JSON']>;
};

export type UpdateSubcategoryInput = {
  subcategoryId: Scalars['String'];
  subcategoryName?: Maybe<Scalars['String']>;
  image?: Maybe<Scalars['FileUpload']>;
  categoryId?: Maybe<Scalars['String']>;
  inputs?: Maybe<Scalars['JSON']>;
};


export type User = {
  __typename?: 'User';
  _id: Scalars['ID'];
  email: Scalars['String'];
  firstName: Scalars['String'];
  lastName: Scalars['String'];
  phone: Scalars['Int'];
  city: Scalars['String'];
  last_seen: Scalars['String'];
  listings?: Maybe<Array<Listing>>;
};

export type UserCreatedPayload = {
  __typename?: 'UserCreatedPayload';
  mutation: MutationType;
  data: User;
};

export type UserPayload = {
  __typename?: 'UserPayload';
  user?: Maybe<User>;
  errors?: Maybe<Array<Error>>;
};

export type UsersPayload = {
  __typename?: 'UsersPayload';
  users?: Maybe<Array<User>>;
  errors?: Maybe<Array<Error>>;
};

export type CategoriesQueryVariables = Exact<{
  categoryId?: Maybe<Scalars['String']>;
}>;


export type CategoriesQuery = (
  { __typename?: 'Query' }
  & { categories?: Maybe<Array<(
    { __typename?: 'Category' }
    & Pick<Category, '_id' | 'categoryName' | 'categoryImage' | 'inputsIdentifier'>
  )>> }
);

export type CategoriesWithSubcategoriesQueryVariables = Exact<{
  categoryId?: Maybe<Scalars['String']>;
}>;


export type CategoriesWithSubcategoriesQuery = (
  { __typename?: 'Query' }
  & { categories?: Maybe<Array<(
    { __typename?: 'Category' }
    & Pick<Category, '_id' | 'categoryName' | 'categoryImage' | 'inputsIdentifier'>
    & { subcategories?: Maybe<Array<(
      { __typename?: 'Subcategory' }
      & Pick<Subcategory, '_id' | 'subcategoryName' | 'subcategoryImage' | 'inputsIdentifier'>
    )>> }
  )>> }
);

export type CategoryWithSubcategoriesQueryVariables = Exact<{
  categoryId?: Maybe<Scalars['String']>;
}>;


export type CategoryWithSubcategoriesQuery = (
  { __typename?: 'Query' }
  & { categories?: Maybe<Array<(
    { __typename?: 'Category' }
    & Pick<Category, '_id' | 'categoryName' | 'categoryImage'>
    & { subcategories?: Maybe<Array<(
      { __typename?: 'Subcategory' }
      & Pick<Subcategory, '_id' | 'subcategoryName' | 'subcategoryImage'>
    )>> }
  )>> }
);

export type CreateCategoryMutationVariables = Exact<{
  categoryName: Scalars['String'];
  image: Scalars['FileUpload'];
  inputs?: Maybe<Scalars['JSON']>;
}>;


export type CreateCategoryMutation = (
  { __typename?: 'Mutation' }
  & { createCategory: (
    { __typename?: 'Category' }
    & Pick<Category, '_id' | 'categoryName' | 'categoryImage' | 'inputs'>
  ) }
);

export type CreateListingMutationVariables = Exact<{
  name: Scalars['String'];
  description?: Maybe<Scalars['String']>;
  price: Scalars['Int'];
  currency: Scalars['String'];
  images?: Maybe<Array<Scalars['FileUpload']> | Scalars['FileUpload']>;
  details?: Maybe<Scalars['JSON']>;
  category: Scalars['String'];
  subcategory: Scalars['String'];
}>;


export type CreateListingMutation = (
  { __typename?: 'Mutation' }
  & { createListing: (
    { __typename?: 'Listing' }
    & Pick<Listing, '_id' | 'name'>
  ) }
);

export type CreateSubcategoryMutationVariables = Exact<{
  id: Scalars['String'];
  subcategoryName: Scalars['String'];
  image: Scalars['FileUpload'];
  inputs?: Maybe<Scalars['JSON']>;
}>;


export type CreateSubcategoryMutation = (
  { __typename?: 'Mutation' }
  & { createSubcategory: (
    { __typename?: 'Subcategory' }
    & Pick<Subcategory, '_id' | 'subcategoryName' | 'subcategoryImage' | 'inputs'>
  ) }
);

export type CreateUserMutationVariables = Exact<{
  email: Scalars['String'];
  password: Scalars['String'];
  firstName: Scalars['String'];
  lastName: Scalars['String'];
  city: Scalars['String'];
  phone: Scalars['Int'];
}>;


export type CreateUserMutation = (
  { __typename?: 'Mutation' }
  & { createUser: (
    { __typename?: 'AuthPayload' }
    & Pick<AuthPayload, 'token'>
    & { user?: Maybe<(
      { __typename?: 'User' }
      & Pick<User, '_id' | 'email'>
    )>, errors?: Maybe<Array<(
      { __typename?: 'Error' }
      & Pick<Error, 'path' | 'message'>
    )>> }
  ) }
);

export type DeleteCategoryMutationVariables = Exact<{
  id: Scalars['String'];
}>;


export type DeleteCategoryMutation = (
  { __typename?: 'Mutation' }
  & Pick<Mutation, 'deleteCategory'>
);

export type DeleteSubcategoryMutationVariables = Exact<{
  id: Scalars['String'];
}>;


export type DeleteSubcategoryMutation = (
  { __typename?: 'Mutation' }
  & Pick<Mutation, 'deleteSubcategory'>
);

export type GetAddListingInputsQueryVariables = Exact<{
  categoryInputId?: Maybe<Scalars['String']>;
  subcategoryInputId?: Maybe<Scalars['String']>;
}>;


export type GetAddListingInputsQuery = (
  { __typename?: 'Query' }
  & Pick<Query, 'getAddListingInputs'>
);

export type GetCitiesQueryVariables = Exact<{
  countryCode: Scalars['String'];
}>;


export type GetCitiesQuery = (
  { __typename?: 'Query' }
  & Pick<Query, 'getCities'>
);

export type ListingsQueryVariables = Exact<{
  listingId?: Maybe<Scalars['String']>;
  categoryId?: Maybe<Scalars['String']>;
  subcategoryId?: Maybe<Scalars['String']>;
  name?: Maybe<Scalars['String']>;
  details?: Maybe<Scalars['JSON']>;
}>;


export type ListingsQuery = (
  { __typename?: 'Query' }
  & { listings?: Maybe<Array<(
    { __typename?: 'Listing' }
    & Pick<Listing, '_id' | 'name' | 'description' | 'price' | 'currency' | 'images'>
    & { owner: (
      { __typename?: 'User' }
      & Pick<User, '_id' | 'email'>
    ) }
  )>> }
);

export type LoginMutationVariables = Exact<{
  loginEmail: Scalars['String'];
  loginPassword: Scalars['String'];
}>;


export type LoginMutation = (
  { __typename?: 'Mutation' }
  & { loginUser: (
    { __typename?: 'AuthPayload' }
    & Pick<AuthPayload, 'token'>
    & { user?: Maybe<(
      { __typename?: 'User' }
      & Pick<User, '_id' | 'email'>
    )>, errors?: Maybe<Array<(
      { __typename?: 'Error' }
      & Pick<Error, 'path' | 'message'>
    )>> }
  ) }
);

export type MeQueryVariables = Exact<{ [key: string]: never; }>;


export type MeQuery = (
  { __typename?: 'Query' }
  & { me: (
    { __typename?: 'UserPayload' }
    & { user?: Maybe<(
      { __typename?: 'User' }
      & Pick<User, '_id' | 'email' | 'last_seen'>
    )>, errors?: Maybe<Array<(
      { __typename?: 'Error' }
      & Pick<Error, 'path' | 'message'>
    )>> }
  ) }
);

export type SubcategoriesQueryVariables = Exact<{
  categoryId?: Maybe<Scalars['String']>;
}>;


export type SubcategoriesQuery = (
  { __typename?: 'Query' }
  & { subcategories?: Maybe<Array<(
    { __typename?: 'Subcategory' }
    & Pick<Subcategory, '_id' | 'subcategoryName' | 'subcategoryImage' | 'inputsIdentifier' | 'inputs'>
    & { category: (
      { __typename?: 'Category' }
      & Pick<Category, '_id' | 'categoryName' | 'categoryImage' | 'inputs'>
    ) }
  )>> }
);

export type TestQueryVariables = Exact<{ [key: string]: never; }>;


export type TestQuery = (
  { __typename?: 'Query' }
  & Pick<Query, 'test'>
);

export type UsersQueryVariables = Exact<{ [key: string]: never; }>;


export type UsersQuery = (
  { __typename?: 'Query' }
  & { users?: Maybe<Array<(
    { __typename?: 'UsersPayload' }
    & { users?: Maybe<Array<(
      { __typename?: 'User' }
      & Pick<User, '_id' | 'email'>
    )>>, errors?: Maybe<Array<(
      { __typename?: 'Error' }
      & Pick<Error, 'path' | 'message'>
    )>> }
  )>> }
);


export const CategoriesDocument = gql`
    query categories($categoryId: String) {
  categories(categoryId: $categoryId) {
    _id
    categoryName
    categoryImage
    inputsIdentifier
  }
}
    `;

/**
 * __useCategoriesQuery__
 *
 * To run a query within a React component, call `useCategoriesQuery` and pass it any options that fit your needs.
 * When your component renders, `useCategoriesQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useCategoriesQuery({
 *   variables: {
 *      categoryId: // value for 'categoryId'
 *   },
 * });
 */
export function useCategoriesQuery(baseOptions?: Apollo.QueryHookOptions<CategoriesQuery, CategoriesQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<CategoriesQuery, CategoriesQueryVariables>(CategoriesDocument, options);
      }
export function useCategoriesLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<CategoriesQuery, CategoriesQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<CategoriesQuery, CategoriesQueryVariables>(CategoriesDocument, options);
        }
export type CategoriesQueryHookResult = ReturnType<typeof useCategoriesQuery>;
export type CategoriesLazyQueryHookResult = ReturnType<typeof useCategoriesLazyQuery>;
export type CategoriesQueryResult = Apollo.QueryResult<CategoriesQuery, CategoriesQueryVariables>;
export const CategoriesWithSubcategoriesDocument = gql`
    query categoriesWithSubcategories($categoryId: String) {
  categories(categoryId: $categoryId) {
    _id
    categoryName
    categoryImage
    inputsIdentifier
    subcategories {
      _id
      subcategoryName
      subcategoryImage
      inputsIdentifier
    }
  }
}
    `;

/**
 * __useCategoriesWithSubcategoriesQuery__
 *
 * To run a query within a React component, call `useCategoriesWithSubcategoriesQuery` and pass it any options that fit your needs.
 * When your component renders, `useCategoriesWithSubcategoriesQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useCategoriesWithSubcategoriesQuery({
 *   variables: {
 *      categoryId: // value for 'categoryId'
 *   },
 * });
 */
export function useCategoriesWithSubcategoriesQuery(baseOptions?: Apollo.QueryHookOptions<CategoriesWithSubcategoriesQuery, CategoriesWithSubcategoriesQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<CategoriesWithSubcategoriesQuery, CategoriesWithSubcategoriesQueryVariables>(CategoriesWithSubcategoriesDocument, options);
      }
export function useCategoriesWithSubcategoriesLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<CategoriesWithSubcategoriesQuery, CategoriesWithSubcategoriesQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<CategoriesWithSubcategoriesQuery, CategoriesWithSubcategoriesQueryVariables>(CategoriesWithSubcategoriesDocument, options);
        }
export type CategoriesWithSubcategoriesQueryHookResult = ReturnType<typeof useCategoriesWithSubcategoriesQuery>;
export type CategoriesWithSubcategoriesLazyQueryHookResult = ReturnType<typeof useCategoriesWithSubcategoriesLazyQuery>;
export type CategoriesWithSubcategoriesQueryResult = Apollo.QueryResult<CategoriesWithSubcategoriesQuery, CategoriesWithSubcategoriesQueryVariables>;
export const CategoryWithSubcategoriesDocument = gql`
    query categoryWithSubcategories($categoryId: String) {
  categories(categoryId: $categoryId) {
    _id
    categoryName
    categoryImage
    subcategories {
      _id
      subcategoryName
      subcategoryImage
    }
  }
}
    `;

/**
 * __useCategoryWithSubcategoriesQuery__
 *
 * To run a query within a React component, call `useCategoryWithSubcategoriesQuery` and pass it any options that fit your needs.
 * When your component renders, `useCategoryWithSubcategoriesQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useCategoryWithSubcategoriesQuery({
 *   variables: {
 *      categoryId: // value for 'categoryId'
 *   },
 * });
 */
export function useCategoryWithSubcategoriesQuery(baseOptions?: Apollo.QueryHookOptions<CategoryWithSubcategoriesQuery, CategoryWithSubcategoriesQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<CategoryWithSubcategoriesQuery, CategoryWithSubcategoriesQueryVariables>(CategoryWithSubcategoriesDocument, options);
      }
export function useCategoryWithSubcategoriesLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<CategoryWithSubcategoriesQuery, CategoryWithSubcategoriesQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<CategoryWithSubcategoriesQuery, CategoryWithSubcategoriesQueryVariables>(CategoryWithSubcategoriesDocument, options);
        }
export type CategoryWithSubcategoriesQueryHookResult = ReturnType<typeof useCategoryWithSubcategoriesQuery>;
export type CategoryWithSubcategoriesLazyQueryHookResult = ReturnType<typeof useCategoryWithSubcategoriesLazyQuery>;
export type CategoryWithSubcategoriesQueryResult = Apollo.QueryResult<CategoryWithSubcategoriesQuery, CategoryWithSubcategoriesQueryVariables>;
export const CreateCategoryDocument = gql`
    mutation createCategory($categoryName: String!, $image: FileUpload!, $inputs: JSON) {
  createCategory(
    data: {categoryName: $categoryName, image: $image, inputs: $inputs}
  ) {
    _id
    categoryName
    categoryImage
    inputs
  }
}
    `;
export type CreateCategoryMutationFn = Apollo.MutationFunction<CreateCategoryMutation, CreateCategoryMutationVariables>;

/**
 * __useCreateCategoryMutation__
 *
 * To run a mutation, you first call `useCreateCategoryMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useCreateCategoryMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [createCategoryMutation, { data, loading, error }] = useCreateCategoryMutation({
 *   variables: {
 *      categoryName: // value for 'categoryName'
 *      image: // value for 'image'
 *      inputs: // value for 'inputs'
 *   },
 * });
 */
export function useCreateCategoryMutation(baseOptions?: Apollo.MutationHookOptions<CreateCategoryMutation, CreateCategoryMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<CreateCategoryMutation, CreateCategoryMutationVariables>(CreateCategoryDocument, options);
      }
export type CreateCategoryMutationHookResult = ReturnType<typeof useCreateCategoryMutation>;
export type CreateCategoryMutationResult = Apollo.MutationResult<CreateCategoryMutation>;
export type CreateCategoryMutationOptions = Apollo.BaseMutationOptions<CreateCategoryMutation, CreateCategoryMutationVariables>;
export const CreateListingDocument = gql`
    mutation createListing($name: String!, $description: String, $price: Int!, $currency: String!, $images: [FileUpload!], $details: JSON, $category: String!, $subcategory: String!) {
  createListing(
    data: {name: $name, description: $description, price: $price, currency: $currency, images: $images, details: $details, category: $category, subcategory: $subcategory}
  ) {
    _id
    name
  }
}
    `;
export type CreateListingMutationFn = Apollo.MutationFunction<CreateListingMutation, CreateListingMutationVariables>;

/**
 * __useCreateListingMutation__
 *
 * To run a mutation, you first call `useCreateListingMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useCreateListingMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [createListingMutation, { data, loading, error }] = useCreateListingMutation({
 *   variables: {
 *      name: // value for 'name'
 *      description: // value for 'description'
 *      price: // value for 'price'
 *      currency: // value for 'currency'
 *      images: // value for 'images'
 *      details: // value for 'details'
 *      category: // value for 'category'
 *      subcategory: // value for 'subcategory'
 *   },
 * });
 */
export function useCreateListingMutation(baseOptions?: Apollo.MutationHookOptions<CreateListingMutation, CreateListingMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<CreateListingMutation, CreateListingMutationVariables>(CreateListingDocument, options);
      }
export type CreateListingMutationHookResult = ReturnType<typeof useCreateListingMutation>;
export type CreateListingMutationResult = Apollo.MutationResult<CreateListingMutation>;
export type CreateListingMutationOptions = Apollo.BaseMutationOptions<CreateListingMutation, CreateListingMutationVariables>;
export const CreateSubcategoryDocument = gql`
    mutation createSubcategory($id: String!, $subcategoryName: String!, $image: FileUpload!, $inputs: JSON) {
  createSubcategory(
    data: {categoryId: $id, subcategoryName: $subcategoryName, image: $image, inputs: $inputs}
  ) {
    _id
    subcategoryName
    subcategoryImage
    inputs
  }
}
    `;
export type CreateSubcategoryMutationFn = Apollo.MutationFunction<CreateSubcategoryMutation, CreateSubcategoryMutationVariables>;

/**
 * __useCreateSubcategoryMutation__
 *
 * To run a mutation, you first call `useCreateSubcategoryMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useCreateSubcategoryMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [createSubcategoryMutation, { data, loading, error }] = useCreateSubcategoryMutation({
 *   variables: {
 *      id: // value for 'id'
 *      subcategoryName: // value for 'subcategoryName'
 *      image: // value for 'image'
 *      inputs: // value for 'inputs'
 *   },
 * });
 */
export function useCreateSubcategoryMutation(baseOptions?: Apollo.MutationHookOptions<CreateSubcategoryMutation, CreateSubcategoryMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<CreateSubcategoryMutation, CreateSubcategoryMutationVariables>(CreateSubcategoryDocument, options);
      }
export type CreateSubcategoryMutationHookResult = ReturnType<typeof useCreateSubcategoryMutation>;
export type CreateSubcategoryMutationResult = Apollo.MutationResult<CreateSubcategoryMutation>;
export type CreateSubcategoryMutationOptions = Apollo.BaseMutationOptions<CreateSubcategoryMutation, CreateSubcategoryMutationVariables>;
export const CreateUserDocument = gql`
    mutation CreateUser($email: String!, $password: String!, $firstName: String!, $lastName: String!, $city: String!, $phone: Int!) {
  createUser(
    data: {email: $email, password: $password, firstName: $firstName, lastName: $lastName, city: $city, phone: $phone}
  ) {
    user {
      _id
      email
    }
    token
    errors {
      path
      message
    }
  }
}
    `;
export type CreateUserMutationFn = Apollo.MutationFunction<CreateUserMutation, CreateUserMutationVariables>;

/**
 * __useCreateUserMutation__
 *
 * To run a mutation, you first call `useCreateUserMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useCreateUserMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [createUserMutation, { data, loading, error }] = useCreateUserMutation({
 *   variables: {
 *      email: // value for 'email'
 *      password: // value for 'password'
 *      firstName: // value for 'firstName'
 *      lastName: // value for 'lastName'
 *      city: // value for 'city'
 *      phone: // value for 'phone'
 *   },
 * });
 */
export function useCreateUserMutation(baseOptions?: Apollo.MutationHookOptions<CreateUserMutation, CreateUserMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<CreateUserMutation, CreateUserMutationVariables>(CreateUserDocument, options);
      }
export type CreateUserMutationHookResult = ReturnType<typeof useCreateUserMutation>;
export type CreateUserMutationResult = Apollo.MutationResult<CreateUserMutation>;
export type CreateUserMutationOptions = Apollo.BaseMutationOptions<CreateUserMutation, CreateUserMutationVariables>;
export const DeleteCategoryDocument = gql`
    mutation deleteCategory($id: String!) {
  deleteCategory(categoryId: $id)
}
    `;
export type DeleteCategoryMutationFn = Apollo.MutationFunction<DeleteCategoryMutation, DeleteCategoryMutationVariables>;

/**
 * __useDeleteCategoryMutation__
 *
 * To run a mutation, you first call `useDeleteCategoryMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useDeleteCategoryMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [deleteCategoryMutation, { data, loading, error }] = useDeleteCategoryMutation({
 *   variables: {
 *      id: // value for 'id'
 *   },
 * });
 */
export function useDeleteCategoryMutation(baseOptions?: Apollo.MutationHookOptions<DeleteCategoryMutation, DeleteCategoryMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<DeleteCategoryMutation, DeleteCategoryMutationVariables>(DeleteCategoryDocument, options);
      }
export type DeleteCategoryMutationHookResult = ReturnType<typeof useDeleteCategoryMutation>;
export type DeleteCategoryMutationResult = Apollo.MutationResult<DeleteCategoryMutation>;
export type DeleteCategoryMutationOptions = Apollo.BaseMutationOptions<DeleteCategoryMutation, DeleteCategoryMutationVariables>;
export const DeleteSubcategoryDocument = gql`
    mutation deleteSubcategory($id: String!) {
  deleteSubcategory(subcategoryId: $id)
}
    `;
export type DeleteSubcategoryMutationFn = Apollo.MutationFunction<DeleteSubcategoryMutation, DeleteSubcategoryMutationVariables>;

/**
 * __useDeleteSubcategoryMutation__
 *
 * To run a mutation, you first call `useDeleteSubcategoryMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useDeleteSubcategoryMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [deleteSubcategoryMutation, { data, loading, error }] = useDeleteSubcategoryMutation({
 *   variables: {
 *      id: // value for 'id'
 *   },
 * });
 */
export function useDeleteSubcategoryMutation(baseOptions?: Apollo.MutationHookOptions<DeleteSubcategoryMutation, DeleteSubcategoryMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<DeleteSubcategoryMutation, DeleteSubcategoryMutationVariables>(DeleteSubcategoryDocument, options);
      }
export type DeleteSubcategoryMutationHookResult = ReturnType<typeof useDeleteSubcategoryMutation>;
export type DeleteSubcategoryMutationResult = Apollo.MutationResult<DeleteSubcategoryMutation>;
export type DeleteSubcategoryMutationOptions = Apollo.BaseMutationOptions<DeleteSubcategoryMutation, DeleteSubcategoryMutationVariables>;
export const GetAddListingInputsDocument = gql`
    query getAddListingInputs($categoryInputId: String, $subcategoryInputId: String) {
  getAddListingInputs(
    categoryInputId: $categoryInputId
    subcategoryInputId: $subcategoryInputId
  )
}
    `;

/**
 * __useGetAddListingInputsQuery__
 *
 * To run a query within a React component, call `useGetAddListingInputsQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetAddListingInputsQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetAddListingInputsQuery({
 *   variables: {
 *      categoryInputId: // value for 'categoryInputId'
 *      subcategoryInputId: // value for 'subcategoryInputId'
 *   },
 * });
 */
export function useGetAddListingInputsQuery(baseOptions?: Apollo.QueryHookOptions<GetAddListingInputsQuery, GetAddListingInputsQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<GetAddListingInputsQuery, GetAddListingInputsQueryVariables>(GetAddListingInputsDocument, options);
      }
export function useGetAddListingInputsLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<GetAddListingInputsQuery, GetAddListingInputsQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<GetAddListingInputsQuery, GetAddListingInputsQueryVariables>(GetAddListingInputsDocument, options);
        }
export type GetAddListingInputsQueryHookResult = ReturnType<typeof useGetAddListingInputsQuery>;
export type GetAddListingInputsLazyQueryHookResult = ReturnType<typeof useGetAddListingInputsLazyQuery>;
export type GetAddListingInputsQueryResult = Apollo.QueryResult<GetAddListingInputsQuery, GetAddListingInputsQueryVariables>;
export const GetCitiesDocument = gql`
    query getCities($countryCode: String!) {
  getCities(countryCode: $countryCode)
}
    `;

/**
 * __useGetCitiesQuery__
 *
 * To run a query within a React component, call `useGetCitiesQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetCitiesQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetCitiesQuery({
 *   variables: {
 *      countryCode: // value for 'countryCode'
 *   },
 * });
 */
export function useGetCitiesQuery(baseOptions: Apollo.QueryHookOptions<GetCitiesQuery, GetCitiesQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<GetCitiesQuery, GetCitiesQueryVariables>(GetCitiesDocument, options);
      }
export function useGetCitiesLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<GetCitiesQuery, GetCitiesQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<GetCitiesQuery, GetCitiesQueryVariables>(GetCitiesDocument, options);
        }
export type GetCitiesQueryHookResult = ReturnType<typeof useGetCitiesQuery>;
export type GetCitiesLazyQueryHookResult = ReturnType<typeof useGetCitiesLazyQuery>;
export type GetCitiesQueryResult = Apollo.QueryResult<GetCitiesQuery, GetCitiesQueryVariables>;
export const ListingsDocument = gql`
    query listings($listingId: String, $categoryId: String, $subcategoryId: String, $name: String, $details: JSON) {
  listings(
    data: {listingId: $listingId, categoryId: $categoryId, subcategoryId: $subcategoryId, name: $name, details: $details}
  ) {
    _id
    name
    description
    price
    currency
    images
    owner {
      _id
      email
    }
  }
}
    `;

/**
 * __useListingsQuery__
 *
 * To run a query within a React component, call `useListingsQuery` and pass it any options that fit your needs.
 * When your component renders, `useListingsQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useListingsQuery({
 *   variables: {
 *      listingId: // value for 'listingId'
 *      categoryId: // value for 'categoryId'
 *      subcategoryId: // value for 'subcategoryId'
 *      name: // value for 'name'
 *      details: // value for 'details'
 *   },
 * });
 */
export function useListingsQuery(baseOptions?: Apollo.QueryHookOptions<ListingsQuery, ListingsQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<ListingsQuery, ListingsQueryVariables>(ListingsDocument, options);
      }
export function useListingsLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<ListingsQuery, ListingsQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<ListingsQuery, ListingsQueryVariables>(ListingsDocument, options);
        }
export type ListingsQueryHookResult = ReturnType<typeof useListingsQuery>;
export type ListingsLazyQueryHookResult = ReturnType<typeof useListingsLazyQuery>;
export type ListingsQueryResult = Apollo.QueryResult<ListingsQuery, ListingsQueryVariables>;
export const LoginDocument = gql`
    mutation Login($loginEmail: String!, $loginPassword: String!) {
  loginUser(email: $loginEmail, password: $loginPassword) {
    user {
      _id
      email
    }
    token
    errors {
      path
      message
    }
  }
}
    `;
export type LoginMutationFn = Apollo.MutationFunction<LoginMutation, LoginMutationVariables>;

/**
 * __useLoginMutation__
 *
 * To run a mutation, you first call `useLoginMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useLoginMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [loginMutation, { data, loading, error }] = useLoginMutation({
 *   variables: {
 *      loginEmail: // value for 'loginEmail'
 *      loginPassword: // value for 'loginPassword'
 *   },
 * });
 */
export function useLoginMutation(baseOptions?: Apollo.MutationHookOptions<LoginMutation, LoginMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<LoginMutation, LoginMutationVariables>(LoginDocument, options);
      }
export type LoginMutationHookResult = ReturnType<typeof useLoginMutation>;
export type LoginMutationResult = Apollo.MutationResult<LoginMutation>;
export type LoginMutationOptions = Apollo.BaseMutationOptions<LoginMutation, LoginMutationVariables>;
export const MeDocument = gql`
    query Me {
  me {
    user {
      _id
      email
      last_seen
    }
    errors {
      path
      message
    }
  }
}
    `;

/**
 * __useMeQuery__
 *
 * To run a query within a React component, call `useMeQuery` and pass it any options that fit your needs.
 * When your component renders, `useMeQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useMeQuery({
 *   variables: {
 *   },
 * });
 */
export function useMeQuery(baseOptions?: Apollo.QueryHookOptions<MeQuery, MeQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<MeQuery, MeQueryVariables>(MeDocument, options);
      }
export function useMeLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<MeQuery, MeQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<MeQuery, MeQueryVariables>(MeDocument, options);
        }
export type MeQueryHookResult = ReturnType<typeof useMeQuery>;
export type MeLazyQueryHookResult = ReturnType<typeof useMeLazyQuery>;
export type MeQueryResult = Apollo.QueryResult<MeQuery, MeQueryVariables>;
export const SubcategoriesDocument = gql`
    query subcategories($categoryId: String) {
  subcategories(categoryId: $categoryId) {
    _id
    subcategoryName
    subcategoryImage
    inputsIdentifier
    inputs
    category {
      _id
      categoryName
      categoryImage
      inputs
    }
  }
}
    `;

/**
 * __useSubcategoriesQuery__
 *
 * To run a query within a React component, call `useSubcategoriesQuery` and pass it any options that fit your needs.
 * When your component renders, `useSubcategoriesQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useSubcategoriesQuery({
 *   variables: {
 *      categoryId: // value for 'categoryId'
 *   },
 * });
 */
export function useSubcategoriesQuery(baseOptions?: Apollo.QueryHookOptions<SubcategoriesQuery, SubcategoriesQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<SubcategoriesQuery, SubcategoriesQueryVariables>(SubcategoriesDocument, options);
      }
export function useSubcategoriesLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<SubcategoriesQuery, SubcategoriesQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<SubcategoriesQuery, SubcategoriesQueryVariables>(SubcategoriesDocument, options);
        }
export type SubcategoriesQueryHookResult = ReturnType<typeof useSubcategoriesQuery>;
export type SubcategoriesLazyQueryHookResult = ReturnType<typeof useSubcategoriesLazyQuery>;
export type SubcategoriesQueryResult = Apollo.QueryResult<SubcategoriesQuery, SubcategoriesQueryVariables>;
export const TestDocument = gql`
    query Test {
  test
}
    `;

/**
 * __useTestQuery__
 *
 * To run a query within a React component, call `useTestQuery` and pass it any options that fit your needs.
 * When your component renders, `useTestQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useTestQuery({
 *   variables: {
 *   },
 * });
 */
export function useTestQuery(baseOptions?: Apollo.QueryHookOptions<TestQuery, TestQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<TestQuery, TestQueryVariables>(TestDocument, options);
      }
export function useTestLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<TestQuery, TestQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<TestQuery, TestQueryVariables>(TestDocument, options);
        }
export type TestQueryHookResult = ReturnType<typeof useTestQuery>;
export type TestLazyQueryHookResult = ReturnType<typeof useTestLazyQuery>;
export type TestQueryResult = Apollo.QueryResult<TestQuery, TestQueryVariables>;
export const UsersDocument = gql`
    query Users {
  users {
    users {
      _id
      email
    }
    errors {
      path
      message
    }
  }
}
    `;

/**
 * __useUsersQuery__
 *
 * To run a query within a React component, call `useUsersQuery` and pass it any options that fit your needs.
 * When your component renders, `useUsersQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useUsersQuery({
 *   variables: {
 *   },
 * });
 */
export function useUsersQuery(baseOptions?: Apollo.QueryHookOptions<UsersQuery, UsersQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<UsersQuery, UsersQueryVariables>(UsersDocument, options);
      }
export function useUsersLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<UsersQuery, UsersQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<UsersQuery, UsersQueryVariables>(UsersDocument, options);
        }
export type UsersQueryHookResult = ReturnType<typeof useUsersQuery>;
export type UsersLazyQueryHookResult = ReturnType<typeof useUsersLazyQuery>;
export type UsersQueryResult = Apollo.QueryResult<UsersQuery, UsersQueryVariables>;