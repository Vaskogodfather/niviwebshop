import React, {useState} from "react";
import {Button, Checkbox, FormControl, FormControlLabel, FormGroup, Slider} from "@material-ui/core";
import {generateSearchFormFromJSON} from "../../handlers/searchSidebarGenerationHandler";

interface Props {
    subcategories?: any[]
}

const initialSearchValues = {
    category: '',
    subcategory: '',
    name: '',
    description: '',
    price: 0,
    currency: '',
    details: {}
};

export const ListingsSidebar: React.FC<Props> = ({subcategories}) => {

    const [listingValues, setListingValues] = useState(initialSearchValues);

    // console.log(subcategories ? JSON.parse(subcategories[0].inputs) : "");
    let customSearchInputs: any = [];

    function renderDetails(){
            customSearchInputs = generateSearchFormFromJSON(listingValues, setListingValues, subcategories);
            return
    };

    function valuetext(value: number) {
        return `${value}`;
    }

    const [value, setValue] = React.useState<number[]>([20, 37]);
    const handleDaChange = (event: any, newValue: number | number[]) => {
        setValue(newValue as number[]);
    };

    const handleChange = () => {

    };

    return (
        <div className="ads-results-container__sidebar">
            <div className="ads-results-container__sidebar__search">
                <input className="ads-results-container__sidebar__search__input" type="text" name="Search"
                       placeholder={subcategories ? subcategories[0]?.category.categoryName : ""}/>
                <button className="ads-results-container__sidebar__search__button">Pretraga</button>
            </div>

            <div className="ads-results-container__sidebar__search">
                <div className="ads-result-checkboxes-container">
                    <p className="ads-result-search-form-text">Kategorije</p>

                    <div className="ads-result-checkboxes">

                        <FormControl>
                            <FormGroup>
                            {
                                subcategories?.map((subcategory: any) => {
                                    return (<div key={subcategory._id} className="checkbox-numbers">
                                        <FormControlLabel
                                            control={<Checkbox checked={false} onChange={handleChange} name="gilad"/>}
                                            label={subcategory.subcategoryName}
                                        />
                                        <p className="checkbox-number">21</p>
                                    </div>)
                                })
                            }
                            </FormGroup>

                            <Button variant="outlined" style={{
                                letterSpacing: '1px',
                                alignSelf: 'center',
                                justifySelf: 'center',
                                marginTop: '1rem',
                                marginBottom: '1rem'
                            }} type="submit">Prikazi više</Button>

                        </FormControl>

                        <p className="ads-result-search-form-text" style={{marginTop: '1rem'}}>Raspon cene</p>

                        <Slider
                            value={value}
                            onChange={handleDaChange}
                            valueLabelDisplay="auto"
                            aria-labelledby="range-slider"
                            getAriaValueText={valuetext}
                        />

                        {renderDetails()}
                        {customSearchInputs}

                    </div>
                    <div className="ads-result-checkboxes">

                        <Button variant="outlined" style={{
                            width: '19rem',
                            marginTop: '1rem',
                            marginLeft: '2rem',
                            marginRight: '2rem',
                            alignSelf: 'center',
                            justifySelf: 'center',
                            letterSpacing: '1px',
                            background: '#8f1eb3',
                            color: 'white'
                        }} type="submit">Primeni filtere</Button>
                    </div>
                </div>
            </div>
        </div>
    )
};

