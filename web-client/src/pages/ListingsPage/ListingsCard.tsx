import React from "react";
import {Button} from "@material-ui/core";
import {Link} from "react-router-dom";

interface Props {
    payload: any
}

export const ListingsCard: React.FC<Props> = ({payload}) => {

    const images = payload.images.length > 0 ? payload.images.split(',') : [];
    // console.log(images);

    return(
        <div className="ads-result-card">
            <div className="ads-result-card__left">
                <img className="ads-result-card__left__image"
                     src={images.length > 0 ? `http://localhost:1111/images/${images[0]}` : require('../../images/utils/placeholder.png')} alt=""/>
            </div>
            <div className="ads-result-card__right">
                <div className="ads-result-card__right__top">
                    <h3 className="ads-result-card__right__top__heading">{payload.name}</h3>
                    <div className="ads-result-card__right__top__info-container">
                        <div className="ads-result-card__right__top__info-container__item">
                            <svg className="ads-result-card__right__top__info-container__item__icon">
                                <use xlinkHref="/images/svg-icons/sprite.svg#icon-location-pin" />
                            </svg>
                            <button className="ads-result-card__right__top__info-container__item__button">Novi Sad</button>
                        </div>
                        <div className="ads-result-card__right__top__info-container__item">
                            <svg className="ads-result-card__right__top__info-container__item__icon">
                                <use xlinkHref="/images/svg-icons/symbol-defs.svg#icon-calendar" />
                            </svg>
                            <button className="ads-result-card__right__top__info-container__item__button-2">Pre 2 dana</button>
                        </div>
                    </div>
                    <p className="ads-result-card__right__text">{payload.description}</p>
                </div>
                <div className="ads-result-card__right__bottom">
                    <h3 className="ads-result-card__right__bottom__price">{payload.price} {payload.currency}</h3>
                    <Link to={`/listings/display/${payload._id}`}>
                        <Button variant="outlined" style={{letterSpacing: '1px'}} type="submit">Detaljnije</Button>
                    </Link>
                </div>
            </div>
        </div>
    )
}
