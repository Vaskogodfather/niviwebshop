import React from "react";

export function CustomSelectItem({
                                     items,
                                     setListingValues,
                                     listingValues,
                                     itemName,
                                     placeholder,
                                     fieldToInvalidate,
                                     errorMessage,
                                     execute}: any) {
    return (
        <div>
        <select id="selectOptions" className="form-control required styled-select">
            <option value=""
                    hidden
            >{placeholder}</option>
            {items.map((item: any) => {
                return <option
                    className="option-grams"
                    key={item.value ? item.value : Math.random()}
                    onClick={(event) => {
                        if(fieldToInvalidate){
                            setListingValues({
                                ...listingValues,
                                [fieldToInvalidate]: '',
                                [itemName]: item.value ? item.value : ''
                            })
                        } else {
                            setListingValues({
                                ...listingValues,
                                [itemName]: item.value ? item.value : ''
                            })
                        }
                        execute ? execute(event) : console.log('Nothing executed')
                    }}
                    value={item.value}>{item.label}</option>
            })}
        </select>
            <p>{errorMessage}</p>
        </div>
    )
}