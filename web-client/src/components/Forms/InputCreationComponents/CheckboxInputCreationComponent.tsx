import { useState } from "react";
import {InputModel} from "../../../entities/InputModel";
import {InputTypes} from "../../../entities/InputTypes";

interface Props {
    addToInputs: any,
    removeFromInputs: any
}

const CheckboxInputCreationComponent: React.FC<Props> = (props) => {

    const [inputName, setInputName] = useState('');
    const [inputValue, setInputValue] = useState('');
    const [required, setRequired] = useState(false);
    const [searchable, setSearchable] = useState(false);

    const [checkboxListInputs, setCheckboxListInputs] = useState([]);
    const [checkboxListValues, setCheckboxListValues] = useState([]);

    const createInput = () => {
        let input: InputModel = new InputModel(inputName, inputValue, InputTypes.CHECKBOX_LIST, required, searchable, checkboxListValues);
        props.addToInputs(input);
    };

    const removeInput = () => {
        let input: InputModel = new InputModel(inputName, inputValue, InputTypes.CHECKBOX_LIST, required, searchable, checkboxListValues);
        props.removeFromInputs(input);
    };

    return(
        <div className="inputsCreationCard">
            <p className="inputsCreationCard__item">Checkbox input</p>

            <input value={inputName} onChange={e => {
                setInputName(e.target.value);
                setInputValue(e.target.value);
            }} className="inputsCreationCard__item" type="text" placeholder="input name"/>

            <button style={{marginBottom:'1rem'}} onClick={() => {
                let uniqueKey = checkboxListInputs.length;
                let input: any[] = [...checkboxListInputs,
                    <div>
                        <input
                    type="text"
                    value={checkboxListValues[uniqueKey]}
                    onChange={e => {
                        let newValue = e.target.value;
                        let arr: string[] = checkboxListValues;
                        arr[uniqueKey] = newValue;
                        // @ts-ignore
                        setCheckboxListValues(arr);
                        console.log(checkboxListValues);
                    }}
                    className="inputsCreationCard__item"/>

                    </div>];
                // @ts-ignore
                setCheckboxListInputs(input)
            }}>Add item
            </button>

            {checkboxListInputs}
            <div className="inputsCreationCard__item">
                <input checked={required} onChange={() => setRequired(!required)} type="checkbox"
                       name="required"/> Required
            </div>

            <div className="inputsCreationCard__item">
                <input checked={searchable} onChange={() => setSearchable(!searchable)} type="checkbox"
                       name="required"/> Searchable
            </div>

            <button className="inputsCreationCard__item" onClick={() => {
                createInput()
            }}>Confirm input
            </button>

            <button className="inputsCreationCard__item" onClick={() => {
                removeInput()
            }}>Remove input
            </button>

        </div>
    );
};

export default CheckboxInputCreationComponent;