import React, {Fragment} from "react";
import {Link, NavLink} from "react-router-dom";
import {Query} from "../../generated/graphql";
import {Button} from "@material-ui/core";
import {SidebarLoginForm} from "./SidebarLoginForm";

interface Props {
    session: Query,
    refetch: Function
}

export const SideBarComponent: React.FC<Props> = (props) => {

    const {user} = props?.session?.me;

    const AuthHeader =() => (
        <Fragment>
            <div className="sidebar-header">
                <Link to='/create-listing'>
                    <Button variant="outlined" style={{width: '15rem', letterSpacing: '1px'}} type="submit">Kreiraj oglas</Button>
                </Link>

                <Link to='/create-ad'>
                    <Button variant="outlined" style={{width: '15rem', letterSpacing: '1px', marginTop: '1rem', color: 'green'}} type="submit">Moj Profil</Button>
                </Link>

                <Link to='/create-ad'>
                    <Button variant="outlined" style={{width: '15rem',  marginTop: '1rem',  letterSpacing: '1px', color: 'purple'}} type="submit">Omiljene kategorije</Button>
                </Link>

                <Link to='/create-ad'>
                    <Button variant="outlined" style={{width: '15rem',  marginTop: '1rem', color: 'orange', letterSpacing: '1px'}} type="submit">Aktivnosti</Button>
                </Link>
            </div>
        </Fragment>
    );

    let body: JSX.Element | null;

    if (!props.session?.me) { body = null }
    else if (user) { body = <AuthHeader/> }
    else { body = <SidebarLoginForm refetch={props.refetch}/> }

    return (
        <nav className="sidebar">

            <ul className="sidebar-nav">

                <li className="">
                    {body}
                </li>

                <li className="sidebar-nav__item sidebar-nav__item--active">
                    <NavLink to="/" className="sidebar-nav__link">
                        <span className="sidebar-active-item">Oglasi</span>
                    </NavLink>
                </li>

                <li className="sidebar-nav__item">
                    <NavLink to="/games" className="sidebar-nav__link">
                        <span>Prodaja</span>
                    </NavLink>
                </li>

                <li className="sidebar-nav__item">
                    <NavLink to="/games" className="sidebar-nav__link">
                        <span>Usluge</span>
                    </NavLink>
                </li>

                <li className="sidebar-nav__item">
                    <NavLink to="/games" className="sidebar-nav__link">
                        <span>Dostava</span>
                    </NavLink>
                </li>

                <li className="sidebar-nav__item">
                    <NavLink to="/games" className="sidebar-nav__link">
                        <span>Zakazivanje</span>
                    </NavLink>
                </li>

                <li className="sidebar-nav__item">
                    <NavLink to="/settings" className="sidebar-nav__link">
                        <span>Podešavanja</span>
                    </NavLink>
                </li>

                <li className="sidebar-nav__item">
                    <NavLink to="/admin-panel" className="sidebar-nav__link">
                        <span>Admin Panel</span>
                    </NavLink>
                </li>

            </ul>

            <div className="copyright-text">
                &copy; 2019 by NiviemInteractive. All rights reserved.
            </div>
        </nav>
    );
};