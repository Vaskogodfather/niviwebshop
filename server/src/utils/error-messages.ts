export const emailTakenStringError = {
    path: "email",
    message: "Email already taken"
};

export const emailTooShortError = {
    path: "email",
    message: "Email must be at least 3 characters"
};

export const emailTooLongError = {
    path: "email",
    message: "Email too long"
};

export const emailInvalidError = {
    path: "email",
    message: "Email is not valid"
};

export const emailNotConfirmedError = {
    path: "email",
    message: "You must confirm your email in order to login."
};

export const passwordTooShortError = {
    path: "password",
    message: "Password must be at least 6 characters"
};

export const passwordTooLongError = {
    path: "password",
    message: "Password too long"
};

export const userNotAuthenticatedError = {
    path: "auth",
    message: "Unauthorized!"
};

export const invalidCredentialsError = {
    path: "auth",
    message: "Invalid credentials"
};

export const userNotFoundError = {
    path: "user",
    message: "Could not find user"
};

export const expiredKeyError = {
    path: "key",
    message: "Forgot password key expired"
}