import {CarListingInputs} from "./car/Car";
import {ListingModel} from "./shared/Listing";

const CreateListingInputs = [
    {identifier: 'car_inputs', inputs: CarListingInputs}
];

// const SearchListingInputs = [
//     {identifier: 'car_inputs', inputs: CarSearchInputs}
// ];

let inputResult: Array<ListingModel[]> = [];

export const resolveCreateListingInputs = async (inputs:string[]) => {
    inputResult = [];
    await CreateListingInputs.map(input => {
        // console.log(input)
        inputs.find(res => {
            if (input.identifier === res) inputResult.push(input.inputs)
        });
    });
    return inputResult.length !== 0 ? JSON.stringify(inputResult[0]) : null;
};

// export const resolveSearchListingInputs = async (inputs:string[]) => {
//     inputResult = [];
//     await SearchListingInputs.map(input => {
//         inputs.find(res => {
//             if(input.identifier === res) inputResult.push(input.inputs)
//         });
//     });
//     return inputResult.length !== 0 ? JSON.stringify(inputResult[0]) : null;
// };